FROM openjdk:8
ADD target/qlvb-tb-0.0.1-SNAPSHOT.jar qlvb-tb-0.0.1-SNAPSHOT.jar
EXPOSE 9091
ENTRYPOINT ["java", "-jar", "qlvb-tb-0.0.1-SNAPSHOT.jar"]