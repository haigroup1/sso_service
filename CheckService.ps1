If (Get-Service $SERVICE_NAME -ErrorAction SilentlyContinue) {

    If ((Get-Service $SERVICE_NAME).Status -eq 'Running') {

        echo "Service is running "
        Stop-Service $SERVICE_NAME

    } Else {

        echo "Service is stop"

    }

} Else {

    .\CreateService.bat

}
