package com.savis.categories.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
	
	public static Date stringToDate(String dateStr, String oFormat){
		SimpleDateFormat sdf = new SimpleDateFormat(oFormat);
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return date;
	}

	public static Date stringToOnlyDate(String dateStr){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return date;
	}

	//Convert Date to String
	public static String date2str(Date input, String oFormat) {
		String result = "";
		if (input != null) {
			try {
				DateFormat df = new SimpleDateFormat(oFormat);
				result = df.format(input);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
}
