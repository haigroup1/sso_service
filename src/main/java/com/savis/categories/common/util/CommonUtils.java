package com.savis.categories.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.savis.categories.common.security.TokenBasedAuthentication;

/**
 * 
 * @author SonHD
 * @created 16/09/2017
 * 
 * @modified SonHD
 * @modifier 21/09/2017
 * 
 */
public class CommonUtils {
	
	public static String convertFromArrayToString(int[] src) {
		if (src == null || src.length == 0) {
			return "";
		}
		String desc = "" + src[0];
		for (int i = 1; i < src.length; i++) {
			desc = desc + ", " + src[i];
		}
		return desc;
	}
	
	public static Authentication getAuthorization() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication;
	}
	
	public static String getCurrentUserName(){
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			if (userName != null){
				return userName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static Date getCurrentDate(){
		return new Date();
	}
	
	public static String getAccessToken(){
		try {
			TokenBasedAuthentication tokenBasedAuthentication = 
					(TokenBasedAuthentication)SecurityContextHolder.getContext().getAuthentication();
			String token = tokenBasedAuthentication.getToken();
			if(token !=null){
				return token;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static boolean isNullOrEmpty(String str){
		return ((str == null) || (str.equals("")));
	}
	
	public static String standardized(String str) {
        str = str.trim();
        str = str.replaceAll("\\s+", " ");
        return str;
    }
	
//	public static String standardizee(List list) {
//        list = list.trim();
//        list = list.replaceAll("\\s+", " ");
//        return list;
//    }
	
	public static String standardizedCode(String str) {
        str = str.trim();
        str = str.replaceAll(" ", "");
        
        return str;
    }
	
	public static Date getCurrentTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String currentTime = df.format(today);
		
		return today;	
	}
	
	public static String getCurrentStatus(String status) {
		String result = null;
		if (status != null && status.equals("0")) {
			result = "Deactive";
		} else if (status != null && status.equals("1")) {
			result = "Active";
		} else {
			result = "Unknown";
		}
		
		return result;
	}
	
//	public static void toHandleSuccess(Pageable pageable, Page<?> pages, ResponseDataDTO<?> response) throws Exception {
//		// page truyền vào >= tổng số page (dấu bằng vì page tính từ 0)
//		if(pageable.getPageNumber() >= pages.getTotalPages()) {
//			throw new Exception();
//		}
//		
//		SuccessCode(response);
//	}
//	
//	public static void SuccessCode(ResponseDataDTO<?> response) {
//		response.setCode(Constants.SUCCESS_CODE);
//		response.setMessage(Constants.SUCCESS_MSG);
//	}
//	public static void ErrCodeMethodNotAllow(ResponseDataDTO<?> response) {
//		response.setData(null);
//		response.setCode(Constants.ERR_CODE_METHOD_NOT_ALLOW);
//		response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_METHOD_NOT_ALLOW);
//	}
//	
//	public static void ErrCodeNotFound(ResponseDataDTO<?> response) {
//		response.setData(null);
//		response.setCode(Constants.ERR_CODE_NOT_FOUND);
//		response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_NOT_FOUND);
//	}
//	
//	public static void ErrCodeBadRequest(ResponseDataDTO<?> response) {
//		response.setData(null);
//		response.setCode(Constants.ERR_CODE_BAD_REQUEST);
//		response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
//	}
//	
//	public static void ErrCodeForbidden(ResponseDataDTO<?> response) {
//		response.setData(null);
//		response.setCode(Constants.ERR_CODE_FORBIDDEN);
//		response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_FORBIDDEN);
//	}
	
	
	
	
	
	public static String setStatusDefault() {
		return "1";
	}
}
