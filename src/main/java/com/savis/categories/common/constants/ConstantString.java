package com.savis.categories.common.constants;

public class ConstantString {

    public interface Active {
        Long active = 0L;
        Long lock = 1L;
        Long de_active = 2L;

        String active_str = "Đang sử dụng";
        String lock_str = "Đã khóa";
        String de_active_str = "Ngừng sử dụng";

        static String getStatusStr(Long status, String str) {
            if (status == null) {
                return str;
            }
            switch(status.intValue()) {
                case 0:
                    str = Active.active_str;
                    break;
                case 1:
                    str = Active.lock_str;
                    break;
                case 2:
                    str = Active.de_active_str;
                    break;
            }

            return str;
        }
    }

}
