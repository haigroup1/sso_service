package com.savis.categories.common.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class LdapProtocolConfig {
	@Value("${ldap.ad-context.url}")
	private String url;
	
	@Value("${ldap.ad-context.userDn}")
	private String userDn;
	
	@Value("${ldap.ad-context.password}")
	private String password;
	
	@Value("${ldap.ad-context.basePath}")
	private String basePath;
	
	@Bean
	public LdapTemplate ldapTemplate() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(url);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(password);
//		contextSource.setBase(basePath);
		try {
			contextSource.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
		LdapTemplate ldapTemplate = new LdapTemplate();
		ldapTemplate.setContextSource(contextSource);
		return ldapTemplate;
	}
}
