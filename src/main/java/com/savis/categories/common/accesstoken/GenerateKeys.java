package com.savis.categories.common.accesstoken;

import java.security.SecureRandom;


public class GenerateKeys {
	public static String KEY_ALGORITHM = "RSA";
	public static int NUM_BITS = 1024;
	public static int KEY_SIZE = 32;
	
	private static final char[] DEFAULT_CODEC = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
			.toCharArray();

	public static String generateKeys() {
		String secretKeyStr = "";
		try {
			SecureRandom sr = new SecureRandom();
			byte[] verifierBytes = new byte[KEY_SIZE];
			sr.nextBytes(verifierBytes);
			secretKeyStr = getAuthorizationCodeString(verifierBytes);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return secretKeyStr;
	}

	public static String generateClientId() {
		String clientId = "";
		try {
			SecureRandom sr = new SecureRandom();
			byte[] verifierBytes = new byte[KEY_SIZE];
			sr.nextBytes(verifierBytes);
			clientId = getAuthorizationCodeString(verifierBytes);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return clientId;
	}
	
	public static String getAuthorizationCodeString(byte[] verifierBytes) {
		char[] chars = new char[verifierBytes.length];
		for (int i = 0; i < verifierBytes.length; i++) {
			chars[i] = DEFAULT_CODEC[((verifierBytes[i] & 0xFF) % DEFAULT_CODEC.length)];
		}
		return new String(chars);
	}

}
