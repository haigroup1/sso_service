package com.savis.categories.common.exception;

import com.savis.categories.common.dto.ResponseMessage;

/**
 * 
 * @author LongTT
 *
 */
public class SuccessException extends RuntimeException {
	
	/* the response message*/
	private ResponseMessage responseMessage;

	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new BadRequestException entity
	 */
	public SuccessException(){
		
	}
	
	/**
	 * Instantiates a new BadRequestException entity
	 * @param responseMessage: the response message
	 */
	public SuccessException(ResponseMessage responseMessage){
		this.responseMessage = responseMessage;
	}

	/* the getter and setter method*/
	public ResponseMessage getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(ResponseMessage responseMessage) {
		this.responseMessage = responseMessage;
	}
	

}
