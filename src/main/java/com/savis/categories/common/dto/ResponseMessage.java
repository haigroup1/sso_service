package com.savis.categories.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The response message
 * @author LongTT
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage {
	/* the code of the response*/
	private int code;
	/* the sub code of the response*/
	private int subCode;
	/* the message detail of the response*/
	private String detail;
}
