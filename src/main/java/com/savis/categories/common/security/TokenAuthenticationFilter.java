package com.savis.categories.common.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import com.savis.categories.auth.authen.AuthenticationService;
import com.savis.categories.auth.authen.ISUserInfo;
import com.savis.categories.common.constants.CommonConstants;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

	@Value("${jwt.header}")
	private String AUTH_HEADER;

	@Autowired
	TokenHelper tokenHelper;

	@Value("${sso.getUsernameUrl}")
	private String getUsernameUrl;

	@Autowired
	UserDetailsService userDetailServiceImpl;

	@Autowired
	AuthenticationService authenticationService;

	private String getToken(HttpServletRequest request) {

		String accessToken = request.getHeader(CommonConstants.HEADER_FIELD.AUTHORIZATION);
		if (accessToken != null && accessToken.startsWith("Bearer ")) {
			return accessToken.substring(7);
		}
		return null;
	}

	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// Get accessToken from request
		String accessToken = getToken(request);
		if (accessToken != null) {
			// get userName from accessToken
			ISUserInfo iSUserInfo = authenticationService.getUserInfo(accessToken);
			if (iSUserInfo != null) {
				// Get user
				UserDetails userDetails = userDetailServiceImpl.loadUserByUsername(iSUserInfo.getSub());
				// Create authentication
				TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails, true);
				authentication.setToken(accessToken);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			} else {
				SecurityContextHolder.getContext().setAuthentication(new AnonAuthentication());
			}
		} else {
			SecurityContextHolder.getContext().setAuthentication(new AnonAuthentication());
		}
		chain.doFilter(request, response);
	}
}
