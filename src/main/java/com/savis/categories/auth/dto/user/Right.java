package com.savis.categories.auth.dto.user;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Right {

	private int rightId;
	private int parentRightId;
	private String rightCode;
	private String rightName;
	private int status;
	private int rightOrder;
	private int hasChild;
	private String urlRewrite;
	private String iconUrl;
	private String description;
	private int applicationId;
	private List<Access> accesses;
	public int getRightId() {
		return rightId;
	}
	public void setRightId(int rightId) {
		this.rightId = rightId;
	}
	public int getParentRightId() {
		return parentRightId;
	}
	public void setParentRightId(int parentRightId) {
		this.parentRightId = parentRightId;
	}
	public String getRightCode() {
		return rightCode;
	}
	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}
	public String getRightName() {
		return rightName;
	}
	public void setRightName(String rightName) {
		this.rightName = rightName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getRightOrder() {
		return rightOrder;
	}
	public void setRightOrder(int rightOrder) {
		this.rightOrder = rightOrder;
	}
	public int getHasChild() {
		return hasChild;
	}
	public void setHasChild(int hasChild) {
		this.hasChild = hasChild;
	}
	public String getUrlRewrite() {
		return urlRewrite;
	}
	public void setUrlRewrite(String urlRewrite) {
		this.urlRewrite = urlRewrite;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public List<Access> getAccesses() {
		return accesses;
	}
	public void setAccesses(List<Access> accesses) {
		this.accesses = accesses;
	}
	
	

}
