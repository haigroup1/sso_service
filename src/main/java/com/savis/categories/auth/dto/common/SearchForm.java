package com.savis.categories.auth.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchForm {

    private String status;
    private String type;
    private String system;
    private Date fromDate;
    private Date toDate;
    // phân trang hay ko
    private Boolean isPage;
    // search theo ngày
    private Date dateSearch;
    // gửi hoặc nhận
    private Boolean apiSent;
    // order
    private String orderString;
    private String api;
    private Boolean thong_diep_trao_doi = false;

    private Long count_total;
    private Long count_success;
    private String creation_date_sub;
    private Date creation_date;
    private String type_detail;
    private String creation_date_str;
    private String creation_date_sub_str;
    private String source_id;
    private String maHSNhan;
}
