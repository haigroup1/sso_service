package com.savis.categories.auth.dto.user;

import java.util.List;

import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.Application;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

	private String userName;
	private String loweredUsername;
	private String mobileAlias;
	private int isAnonymous;
	private int userType;
	private int applicationId;

	private AccessTokenInfo accessTokenInfo;

	private List<AdmRole> rights;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoweredUsername() {
		return loweredUsername;
	}

	public void setLoweredUsername(String loweredUsername) {
		this.loweredUsername = loweredUsername;
	}

	public String getMobileAlias() {
		return mobileAlias;
	}

	public void setMobileAlias(String mobileAlias) {
		this.mobileAlias = mobileAlias;
	}

	public int getIsAnonymous() {
		return isAnonymous;
	}

	public void setIsAnonymous(int isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public AccessTokenInfo getAccessTokenInfo() {
		return accessTokenInfo;
	}

	public void setAccessTokenInfo(AccessTokenInfo accessTokenInfo) {
		this.accessTokenInfo = accessTokenInfo;
	}

	public List<AdmRole> getRights() {
		return rights;
	}

	public void setRights(List<AdmRole> rights) {
		this.rights = rights;
	}

	
	
}
