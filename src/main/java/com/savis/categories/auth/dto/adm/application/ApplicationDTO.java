package com.savis.categories.auth.dto.adm.application;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDTO {

	private Integer id;
	private String tenApp;
	private String moTa;
	private String donvixuly;
	private Integer[] rightIds;

	public Integer getAppid () {
		return id;
	}

	public void setAppid ( Integer id ) {
		this.id = id;
	}

	public String getTenApp () {
		return tenApp;
	}

	public String getDonvixuly () {
		return donvixuly;
	}

	public void setDonvixuly ( String donvixuly ) {
		this.donvixuly = donvixuly;
	}

	public void setTenApp ( String tenApp ) {
		this.tenApp = tenApp;
	}

	public String getMoTa () {
		return moTa;
	}

	public void setMoTa ( String moTa ) {
		this.moTa = moTa;
	}

	public Integer[] getRightIds () {
		return rightIds;
	}

	public void setRightIds ( Integer[] rightIds ) {
		this.rightIds = rightIds;
	}
}
