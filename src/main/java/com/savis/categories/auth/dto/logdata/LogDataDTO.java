package com.savis.categories.auth.dto.logdata;

import com.savis.categories.dao.entity.view.LogDataView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDataDTO {

    private Page<LogDataView> page;
    private List<LogDataView> list;

}
