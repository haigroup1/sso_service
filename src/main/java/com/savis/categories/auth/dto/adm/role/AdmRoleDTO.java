package com.savis.categories.auth.dto.adm.role;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdmRoleDTO {

	private Integer roleId;
	private String roleName;
	private String loweredRoleName;
	private String roleCode;
	private String description;
	private Integer enableDelete;
	private Integer status;
	private Integer applicationId;
	private Integer[] admAppIds;
//	private Integer[] admAccessRightIds;
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getLoweredRoleName() {
		return loweredRoleName;
	}
	public void setLoweredRoleName(String loweredRoleName) {
		this.loweredRoleName = loweredRoleName;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getEnableDelete() {
		return enableDelete;
	}
	public void setEnableDelete(Integer enableDelete) {
		this.enableDelete = enableDelete;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer[] getAdmAppIds () {
		return admAppIds;
	}

	public void setAdmAppIds ( Integer[] admAppIds ) {
		this.admAppIds = admAppIds;
	}
}
