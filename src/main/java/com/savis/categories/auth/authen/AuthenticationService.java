package com.savis.categories.auth.authen;

public interface AuthenticationService {

	boolean isAuthenticated(String accessToken);

	ISUserInfo getUserInfo(String accessToken);

	ISTokenInfo getAccessTokenByCode(String authorizationCode);

	ISTokenInfo getAccessTokenByRefreshToken(String refreshToken);

}
