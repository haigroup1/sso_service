package com.savis.categories.service;

import com.savis.categories.auth.dto.adm.user.AdmUserDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdmUserService {

	/**
	 * 
	 * @param pageable
	 * @return a {@link Page} of User
	 */
	Page<AdmUser> getPage ( Pageable pageable );

	/**
	 *
	 * @param pageable
	 * @param admUser:
	 *            the search object
	 * @return a {@link Page} of User
	 */
	Page<AdmUser> getPage ( Pageable pageable, AdmUser admUser );

	/**
	 *
	 * @param pageable
	 * @param filter:
	 *            the search keywords
	 * @return a {@link Page} of User
	 */
	Page<AdmUser> getPage ( Pageable pageable, String filter );

	/**
	 *
	 * @return a {@link List} of User
	 */
	List<AdmUser> getList ();

	/**
	 *
	 * @param ApplicationId:
	 *            the application id
	 * @return a {@link List} of User
	 */
	List<AdmUser> getList ( int ApplicationId );

	/**
	 *
	 * @param id:
	 *            the id of the User
	 * @return a {@link AdmUser}
	 */
	AdmUser getOne ( int id );


	Result create ( AdmUserDTO admUserDTO );


	Result update ( AdmUserDTO admUserDTO );

	/**
	 *
	 * @param id:
	 *            the id of the User
	 * @return a {@link Result}
	 */
	Result delete ( int id );

	/**
	 * delete a list User
	 *
	 * @param ids:
	 *            the list id
	 * @return a {@link Result}
	 */
	Result delete ( int[] ids );

//	/**
//	 *
//	 * @param userId:
//	 *            the id of the user
//	 * @return a {@link List} of AdmUserRole
//	 */
//	List<AdmUserRole> getUserRoleByUser ( int userId );
}
