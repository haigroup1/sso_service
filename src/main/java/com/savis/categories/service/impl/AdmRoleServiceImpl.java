package com.savis.categories.service.impl;

import com.savis.categories.auth.dto.adm.role.AdmRoleDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.common.util.CommonUtils;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.AdmRoleApp;
import com.savis.categories.dao.entity.adm.Application;
import com.savis.categories.dao.repository.AdmRoleAppRepository;
import com.savis.categories.dao.repository.AdmRoleRepository;
import com.savis.categories.dao.repository.ApplicationRepository;
import com.savis.categories.dao.specifications.AdmRoleSpecification;
import com.savis.categories.service.AdmRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdmRoleServiceImpl implements AdmRoleService {

    @Autowired
    private AdmRoleRepository admRoleRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private AdmRoleAppRepository admRoleAppRepository;

    @Override
    public Page<AdmRole> getPage(Pageable pageable) {
        Page<AdmRole> admRolePage = null;
        try {
            admRolePage = admRoleRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admRolePage;
    }

    @Override
    public Page<AdmRole> getPage(Pageable pageable, AdmRole admRole) {
        Page<AdmRole> admRolePage = null;
        try {
            admRolePage = admRoleRepository.findAll(AdmRoleSpecification.advanceSearch(admRole), pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admRolePage;
    }

    @Override
    public Page<AdmRole> getPage(Pageable pageable, String filter) {
        Page<AdmRole> admRolePage = null;
        try {
            admRolePage = admRoleRepository.findAll(AdmRoleSpecification.filterSearch(filter), pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admRolePage;
    }

    @Override
    public List<AdmRole> getList() {
        List<AdmRole> admRoleList = null;
        try {
            admRoleList = admRoleRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admRoleList;
    }

    @Override
    public List<AdmRole> getList ( int ApplicationId ) {
        return null;
    }

    @Override
    public AdmRole getOne ( int id ) {
        AdmRole admRole = null;
        try {
            admRole = admRoleRepository.findByRoleId(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admRole;
    }

    @Override
    public Result create ( AdmRoleDTO admRoleDTO ) {
        try {
            AdmRole admRole = this.toEntity(admRoleDTO);
            admRoleRepository.save(admRole);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result update ( AdmRoleDTO admRoleDTO ) {
        try {
            AdmRole admRoleExit = admRoleRepository.findByRoleId(admRoleDTO.getRoleId());
            if (admRoleExit != null) {
                AdmRole admRoleNew = this.toEntity(admRoleDTO);
                try {
                    List<AdmRoleApp> listRoleRightExit = admRoleAppRepository.findByAdmRole(admRoleExit.getRoleId());

                    // remove app in role
                    for(AdmRoleApp temp : listRoleRightExit) {
                        admRoleAppRepository.delete(temp.getId());
                    }

                    AdmRole admUserUpdated = admRoleRepository.save(admRoleNew);
                    /// set and save role-app
//                    if (admRoleDTO.getAdmAppIds() != null) {
//                        if(admRoleDTO.getAdmAppIds().length > 0 ) {
//                            Integer[] roldIds = admRoleDTO.getAdmAppIds();
//                            for (int i = 0; i < roldIds.length; i++) {
//                                AdmRoleApp admRoleApp = new AdmRoleApp();
//                                admRoleApp.setAdmRole(admUserUpdated.getRoleId());
//                                admRoleApp.setAppID(applicationRepository.findOne(roldIds[i]).getId());
//                                admRoleApp.setCreator(CommonUtils.getCurrentUserName());
//                                admRoleApp.setCreated(CommonUtils.getCurrentDate());
//
//                                saveMethod(admRoleApp);
//                            }
//                        }
//                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }

//                admRoleRepository.save(admRoleNew);
                return Result.SUCCESS;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Transactional
    @Modifying
    void saveMethod ( AdmRoleApp admRoleApp ) {
        admRoleAppRepository.save(admRoleApp);
    }

    @Override
    public Result delete(int id) {
        try {
            admRoleRepository.delete(id);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result delete(int[] ids) {
        try {
            List<AdmRole> lAdmRoles = new ArrayList<>();
            for (int i = 0; i < ids.length; i++) {
                lAdmRoles.add(admRoleRepository.findByRoleId(ids[i]));
            }
            admRoleRepository.delete(lAdmRoles);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    private AdmRole toEntity(AdmRoleDTO admRoleDTO) {
        AdmRole admRole = new AdmRole();
        admRole.setRoleId(admRoleDTO.getRoleId());
        admRole.setRoleName(admRoleDTO.getRoleName());
        admRole.setLoweredRoleName(admRoleDTO.getLoweredRoleName());
        admRole.setRoleCode(admRoleDTO.getRoleCode());
        admRole.setDescription(admRoleDTO.getDescription());
        admRole.setEnableDelete(admRoleDTO.getEnableDelete());
        admRole.setStatus(admRoleDTO.getStatus());
        admRole.setApplicationId(admRoleDTO.getApplicationId());

        // set application list
        List<Application> applications = new ArrayList<>();
        Integer[] appIds = admRoleDTO.getAdmAppIds();
        if (appIds != null && appIds.length > 0) {
            for (int i = 0; i < appIds.length; i++) {
                Application applicationToPush = new Application();
                applicationToPush = applicationRepository.findOne(appIds[i]);
                applications.add(applicationToPush);
            }
        }
        admRole.setApplicationList(applications);

        return admRole;
    }
}
