package com.savis.categories.service.impl;

import com.savis.categories.dao.entity.adm.AdmUser;
import com.savis.categories.dao.repository.AdmUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.savis.categories.auth.authen.AuthenticationService;
import com.savis.categories.auth.authen.ISTokenInfo;
import com.savis.categories.auth.authen.ISUserInfo;
import com.savis.categories.auth.dto.user.AccessTokenInfo;
import com.savis.categories.auth.dto.user.UserInfo;
import com.savis.categories.service.IsUserService;

@Service
public class IsUserServiceImpl implements IsUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IsUserServiceImpl.class);

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private AdmUserRepository admUserRepository;

	@Override
	public UserInfo getUserInfo(String authorizationCode) {
		UserInfo userInfo = new UserInfo();

		try {
			// get authentication info
			ISTokenInfo isTokenInfo = authenticationService.getAccessTokenByCode(authorizationCode);
			userInfo.setAccessTokenInfo(toTokenInfo(isTokenInfo));
			// get user info
			ISUserInfo isUserInfo = authenticationService.getUserInfo(isTokenInfo.getAccessToken());
			// get authorization info
			if (isTokenInfo != null && isUserInfo != null) {
				AdmUser admUser = admUserRepository.findByUserName(isUserInfo.getSub());
				// get list right
				if (admUser != null) {
					// set user info
					userInfo.setUserName(admUser.getUserName());
					userInfo.setLoweredUsername(admUser.getLoweredUsername());
					userInfo.setMobileAlias(admUser.getMobileAlias());
					userInfo.setIsAnonymous(admUser.getIsAnonymous());

					// get list access right of user
					userInfo.setRights(admUser.getAdmRoles());
				}
				return userInfo;
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	@Override
	public AccessTokenInfo getAccessToken(String refreshToken) {
		// TODO Auto-generated method stub
		try {
			ISTokenInfo isTokenInfo = authenticationService.getAccessTokenByRefreshToken(refreshToken);
			if (isTokenInfo != null) {
				return toTokenInfo(isTokenInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private AccessTokenInfo toTokenInfo(ISTokenInfo isTokenInfo) {
		AccessTokenInfo tokenInfo = new AccessTokenInfo();
		tokenInfo.setAccessToken(isTokenInfo.getAccessToken());
		tokenInfo.setRefreshToken(isTokenInfo.getRefreshToken());
		tokenInfo.setExpiresIn(isTokenInfo.getExpiresIn());
		tokenInfo.setIdToken(isTokenInfo.getIdToken());
		return tokenInfo;
	}

//	private Right admRightToRight(AdmRight admRight, List<AdmAccessRight> accessRights) {
//		Right right = new Right();
//		right.setRightId(admRight.getRightId());
//		right.setParentRightId(admRight.getParentRightId());
//		right.setRightCode(admRight.getRightCode());
//		right.setRightName(admRight.getRightName());
//		right.setStatus(admRight.getStatus());
//		right.setRightOrder(admRight.getRightOrder());
//		right.setHasChild(admRight.getHasChild());
//		right.setUrlRewrite(admRight.getUrlRewrite());
//		right.setIconUrl(admRight.getIconUrl());
//		right.setDescription(admRight.getDescription());
//
//		// set access
//		/*List<Access> accesses = new ArrayList<>();
//		for (AdmAccessRight accessRight : admRight.getAdmAccessRights()) {
//			if (accessRights.contains(accessRight)) {
//				accesses.add(this.admAccessToAccess(accessRight.getAdmAccessList()));
//			}
//		}
//		right.setAccesses(accesses);*/
//		return right;
//	}

//	private Access admAccessToAccess(AdmAccessList accessList) {
//		Access access = new Access();
//		access.setAccessId(accessList.getAccessId());
//		access.setAccessType(accessList.getAccessType());
//		access.setAccessName(accessList.getAccessName());
//		access.setAccessKey(accessList.getAccessKey());
//		access.setViewOrder(accessList.getViewOrder());
//
//		return access;
//	}
}
