package com.savis.categories.service.impl;

import com.savis.categories.auth.dto.adm.application.ApplicationDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.common.util.CommonUtils;
import com.savis.categories.dao.entity.adm.AdmAppRight;
import com.savis.categories.dao.entity.adm.AdmRight;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.Application;
import com.savis.categories.dao.repository.AdmAppRightRepository;
import com.savis.categories.dao.repository.AdmRightRepository;
import com.savis.categories.dao.repository.ApplicationRepository;
import com.savis.categories.dao.specifications.ApplicationSpecification;
import com.savis.categories.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private ApplicationRepository applicationRespository;

    @Autowired
    private AdmAppRightRepository admAppRightRepository;

    @Autowired
    private AdmRightRepository admRightRepository;

    @Override
    public List<Application> getListApplication () {
        try {
            return applicationRespository.findAll();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<Application> getPageAppliaction ( Application application, Pageable pageable ) {
        Page<Application> admAppPage = null;
        try {
            admAppPage = applicationRespository.findAll(ApplicationSpecification.advanceSearch(application), pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admAppPage;
    }

    @Override
    public Application getOne ( Integer appid ) {
        Application application = null;
        try {
            application = applicationRespository.findOne(appid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return application;
    }


    @Override
    public Result create ( ApplicationDTO application ) {
        try {

            applicationRespository.save(toEntity(application));
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result update ( ApplicationDTO applicationDTO ) {
        try {
            Application appExit = applicationRespository.findOne(applicationDTO.getAppid());
            if (appExit != null) {
                try {
                    List<AdmAppRight> listAppRightExit = admAppRightRepository.findByAppID(appExit.getId());
                    Application applicationToEntity = toEntity(applicationDTO);
                    // remove app in role
                    for(AdmAppRight temp : listAppRightExit) {
                        admAppRightRepository.delete(temp.getId());
                    }

                    Application applicationUpadate = applicationRespository.save(applicationToEntity);
                    /// set and save role-app
//                    if (applicationDTO.getRightIds() != null) {
//                        if(applicationDTO.getRightIds().length > 0 ) {
//                            Integer[] roldIds = applicationDTO.getRightIds();
//                            for (int i = 0; i < roldIds.length; i++) {
//                                AdmAppRight admAppRight = new AdmAppRight();
//                                admAppRight.setRightid(admRightRepository.findOne(roldIds[i]).getRightId());
//                                admAppRight.setAppID(applicationUpadate.getId());
//                                admAppRight.setCreator(CommonUtils.getCurrentUserName());
//                                admAppRight.setCreated(CommonUtils.getCurrentDate());
//
//                                saveMethod(admAppRight);
//                            }
//                        }
//                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }
                return Result.SUCCESS;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result delete ( Integer appid ) {
        try {
            applicationRespository.delete(appid);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result delete ( int[] ids ) {
        try {
            List<Application> lAdmRoles = new ArrayList<>();
            for (int i = 0; i < ids.length; i++) {
                lAdmRoles.add(applicationRespository.findById(ids[i]));
            }
            for(Application application: lAdmRoles) {
                admAppRightRepository.deleteByAppID(application.getId());
                applicationRespository.deleteById(application.getId());
            }
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.BAD_REQUEST;
    }


    @Transactional
    @Modifying
    void saveMethod ( AdmAppRight admAppRight ) {
        admAppRightRepository.save(admAppRight);
    }


    private Application toEntity(ApplicationDTO applicationDTO) {
        Application application = new Application();
        application.setId(applicationDTO.getAppid());
        application.setMoTa(applicationDTO.getMoTa());
        application.setTenApp(applicationDTO.getTenApp());

        // set right
        List<AdmRight> rights = new ArrayList<>();
        Integer[] appIds = applicationDTO.getRightIds();
        if (appIds != null && appIds.length > 0) {
            for (int i = 0; i < appIds.length; i++) {
                AdmRight admRightTemp = new AdmRight();
                admRightTemp = admRightRepository.findOne(appIds[i]);
                rights.add(admRightTemp);
            }
        }
        application.setAdmRights(rights);

        return application;
    }
}
