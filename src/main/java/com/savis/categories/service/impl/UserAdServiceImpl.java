package com.savis.categories.service.impl;

import com.savis.categories.dao.entity.dto.UserAD;
import com.savis.categories.service.UserAdService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.security.Security;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Component
@PropertySource("classpath:application.yml")
public class UserAdServiceImpl implements UserAdService {
	@Value("${ad.adServer}")
	private String adServer;// ex: ldap://127.0.0.1
	@Value("${ad.baseName}")
	private String baseName;// ex: ldap://127.0.0.1
	@Value("${ad.username}")
	private String username;// ex: uid=manager,dc=savis,dc=vn
	@Value("${ad.password}")
	private String password;// ex: 12345a@
	@Value("${ad.hostDC}")
	private String hostDC = "";// ex: dc=savis,dc=vn
	@Value("${ad.baseOu}")
	private String baseOu = "";// ex: ou=soft,dc=esb,dc=lab

	private DirContext ctx = null;
	private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
	private final static String authentication = "simple";

	/**
	 * Method conect AD
	 * 
	 * @return LdapContext
	 */
	private DirContext getDirContext() {
		DirContext ctx = null;
		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
			env.put(Context.SECURITY_AUTHENTICATION, authentication);
			env.put(Context.SECURITY_PRINCIPAL, this.username + "," + this.baseName);
			env.put(Context.SECURITY_CREDENTIALS, password);
			env.put(Context.PROVIDER_URL, adServer);
			if (adServer.contains("ldaps")) {
				env.put(Context.SECURITY_PROTOCOL, "ssl");
				Security.addProvider(null);
				System.setProperty("javax.net.ssl.trustStore",
						System.getProperty("java.home").toString() + "/lib/security/cacerts");
			}
			ctx = new InitialDirContext(env);
			System.out.println("Connection Successful.");
		} catch (NamingException nex) {
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.savis.categories.ad.service.UserAdService#createNewUser(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int createNewUser(String username, String surname, String givenName, String ou) {
		ctx = getDirContext();
		try {
			String distinguishedName = "cn=" + username + "," + ou;
			Attributes newAttributes = new BasicAttributes(true);
			Attribute oc = new BasicAttribute("objectclass");
			oc.add("top");
			oc.add("person");
			oc.add("organizationalperson");
			oc.add("user");
			newAttributes.put(oc);
			newAttributes.put(new BasicAttribute("sAMAccountName", username));
			newAttributes.put(new BasicAttribute("userPrincipalName", username + "@" + adServer));
			newAttributes.put(new BasicAttribute("cn", username));
			newAttributes.put(new BasicAttribute("sn", surname));
			newAttributes.put(new BasicAttribute("givenName", givenName));
			newAttributes.put(new BasicAttribute("displayName", givenName + " " + surname));
			System.out.println("Creating: " + username);
			ctx.createSubcontext(distinguishedName, newAttributes);
			return 1;
		} catch (Exception e) {
			System.out.println("create error: " + e);
			e.printStackTrace();
			System.exit(-1);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.savis.categories.ad.service.UserAdService#updateUser(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int updateUser(String username, String attributeName, String attributeValue, String ou) {
		ctx = getDirContext();
		try {
			System.out.println("updating " + username + "\n");
			ModificationItem[] mods = new ModificationItem[1];
			mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
					new BasicAttribute(attributeName, attributeValue));
			ctx.modifyAttributes("cn=" + username + "," + ou, mods);
			return 1;
		} catch (Exception e) {
			System.out.println(" update error: " + e);
			System.exit(-1);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.savis.categories.ad.service.UserAdService#updateUserPassword(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int updateUserPassword(String username, String password, String ou) {
		ctx = getDirContext();
		try {
			System.out.println("updating password...\n");
			String quotedPassword = "\"" + password + "\"";
			char unicodePwd[] = quotedPassword.toCharArray();
			byte pwdArray[] = new byte[unicodePwd.length * 2];
			for (int i = 0; i < unicodePwd.length; i++) {
				pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
				pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
			}
			System.out.print("encoded password: ");
			for (int i = 0; i < pwdArray.length; i++) {
				System.out.print(pwdArray[i] + " ");
			}
			System.out.println();
			ModificationItem[] mods = new ModificationItem[1];
			mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("UnicodePwd", pwdArray));
			ctx.modifyAttributes("cn=" + username + "," + ou, mods);
			return 1;
		} catch (Exception e) {
			System.out.println("update password error: " + e);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.savis.categories.ad.service.UserAdService#fetchUserAttributes(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public UserAD fetchUserAttributes( String username, String ou) {
		Attributes attributes = null;
		ctx = getDirContext();
		UserAD user = new UserAD();
		try {
			System.out.println("fetching: " + username);
			DirContext o = (DirContext) ctx.lookup("cn=" + username + "," + ou);
			System.out.println("search done\n");
			attributes = o.getAttributes("");
			List<String> objectClass = new ArrayList<>();
			for (NamingEnumeration<?> ae = attributes.getAll(); ae.hasMoreElements();) {
				Attribute attr = (Attribute) ae.next();
				String attrId = attr.getID();
				for (NamingEnumeration<?> vals = attr.getAll(); vals.hasMore();) {
					String thing = vals.next().toString();
					if (attrId.equals("displayName")) {
						user.setDisplayName(thing);
					}
					if (attrId.equals("givenName")) {
						user.setGivenName(thing);
					}
					if (attrId.equals("sAMAccountType")) {
						user.setsAMAccountType(thing);
					}
					if (attrId.equals("primaryGroupID")) {
						user.setPrimaryGroupID(thing);
					}

					if (attrId.equals("primaryGroupID")) {
						user.setPrimaryGroupID(thing);
					}
					if (attrId.equals("objectClass")) {
						objectClass.add(thing);
					}
					if (attrId.equals("badPasswordTime")) {
						user.setBadPasswordTime(thing);
					}
					if (attrId.equals("objectCategory")) {
						user.setObjectCategory(thing);
					}
					if (attrId.equals("cn")) {
						user.setCn(thing);
					}
					if (attrId.equals("sn")) {
						user.setSn(thing);
					}
					if (attrId.equals("userAccountControl")) {
						user.setUserAccountControl(thing);
					}
					if (attrId.equals("userPrincipalName")) {
						user.setUserPrincipalName(thing);
					}
					if (attrId.equals("dSCorePropagationData")) {
						user.setdSCorePropagationData(thing);
					}
					if (attrId.equals("codePage")) {
						user.setCodePage(thing);
					}
					if (attrId.equals("distinguishedName")) {
						user.setDistinguishedName(thing);
					}
					if (attrId.equals("whenChanged")) {
						user.setWhenChanged(thing);
					}
					if (attrId.equals("whenCreated")) {
						user.setWhenCreated(thing);
					}
					if (attrId.equals("pwdLastSet")) {
						user.setPwdLastSet(thing);
					}
					if (attrId.equals("logonCount")) {
						user.setLogonCount(thing);
					}
					if (attrId.equals("accountExpires")) {
						user.setAccountExpires(thing);
					}
					if (attrId.equals("lastLogoff")) {
						user.setLastLogoff(thing);
					}
					if (attrId.equals("lockoutTime")) {
						user.setLockoutTime(thing);
					}
					if (attrId.equals("objectGUID")) {
						user.setObjectGUID(thing);
					}
					if (attrId.equals("lastLogon")) {
						user.setLastLogon(thing);
					}
					if (attrId.equals("uSNChanged")) {
						user.setuSNChanged(thing);
					}
					if (attrId.equals("uSNCreated")) {
						user.setuSNCreated(thing);
					}
					if (attrId.equals("objectSid")) {
						user.setObjectSid(thing);
					}
					user.setObjectClass(objectClass);

				}
			}
		} catch (Exception e) {
			System.out.println(" fetch error: " + e);
		}
		return user;
	}
}
