package com.savis.categories.service.impl;

import com.savis.categories.dao.entity.HoSo;
import com.savis.categories.dao.entity.StatusProfileHistory;
import com.savis.categories.dao.entity.dto.*;
import com.savis.categories.dao.repository.HoSoRepository;
import com.savis.categories.dao.specifications.HoSoSpecification;
import com.savis.categories.service.HoSoService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class HoSoServiceImpl implements HoSoService {

    @Value("${status.dangxuly}")
    private Integer dangXuLyInt;

    @Value("${status.dacoketqua}")
    private Integer daCoKQInt;

    @Value("${status.huy}")
    private Integer huyInt;

    @Value("${delete.fax}")
    private String deleteValue;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


    @Autowired
    private HoSoRepository hoSoRepository;

    @Autowired
    private EntityManager entityManager;

    private Logger log = Logger.getLogger(HoSoServiceImpl.class);

    @Override
    public Page<HoSo> getPage(Pageable pageable) {
        Page<HoSo> hoSos = null;
        try {
            hoSos = hoSoRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hoSos;
    }

    @Override
    public Page<HoSoResponse> getPage(Pageable pageable, HoSoSearchDTO hoSoSearchDTO) {
        Page<HoSoResponse> hoSoPage = null;
        try {
            String query = "SELECT DISTINCT hs.id, hs.mahoso, hs.matthc, hs.kenhthuchien, hs.tendoanhnghiep, hs.chuhoso, hs.email, hs.fax, hs.sodienthoai, hs.ngaytiepnhan, hs.ngayhentra, dmttdvc.tentrangthai,\n" +
                    "hs.ngaytra, hs.hinhthuc, hs.ngayketthucxuly, ap.tenapp, hs.rawjson, hs.documenttype, hs.manoibo, hs.madoanhnghiep, hs.macoquancapdangky, hs.loaihinhdoanhnghiep, hs.trangthaihoso\n" +
                    "FROM hoSo hs \n" +
                    "  INNER JOIN Application ap on hs.AppID=ap.ID\n" +
                    "  INNER JOIN ApplicationTT aptt on hs.AppID=aptt.AppID\n" +
                    "  INNER JOIN DMTrangThaiDVC dmttdvc on dmttdvc.ID = aptt.IDTrangThaiDVC\n" +
                    "WHERE (hs.fax != '"+deleteValue+"' or hs.fax is null) and ( hs.TrangThaiHoSo = aptt.IDTrangThaiDVC or hs.TrangThaiHoSo is null ) and hs.ngaytiepnhan is not null ";
            String queryCount = "SELECT count( DISTINCT hs.id) as tonghoso\n" +
                    "FROM hoSo hs \n" +
                    "  INNER JOIN Application ap on hs.AppID=ap.ID\n" +
                    "  INNER JOIN ApplicationTT aptt on hs.AppID=aptt.AppID\n" +
                    "  INNER JOIN DMTrangThaiDVC dmttdvc on dmttdvc.ID = aptt.IDTrangThaiDVC\n" +
                    "WHERE (hs.fax != '"+deleteValue+"' or hs.fax is null) and ( hs.TrangThaiHoSo = aptt.IDTrangThaiDVC or hs.TrangThaiHoSo is null ) and hs.ngaytiepnhan is not null ";
            if (hoSoSearchDTO != null) {
                if(hoSoSearchDTO.getMaTTHC() != null) {
                    if(!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        query += " AND hs.matthc = '"+hoSoSearchDTO.getMaTTHC()+"'";
                        queryCount += " AND hs.matthc = '"+hoSoSearchDTO.getMaTTHC()+"'";
                    }
                }

                if(hoSoSearchDTO.getChuHoSo() != null) {
                    if(!hoSoSearchDTO.getChuHoSo().isEmpty()) {
                        query += " AND hs.chuhoso like N'%"+hoSoSearchDTO.getChuHoSo()+"%'";
                        queryCount += " AND hs.chuhoso like N'%"+hoSoSearchDTO.getChuHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getMaHoSo() != null) {
                    if(!hoSoSearchDTO.getMaHoSo().isEmpty()) {
                        query += " AND hs.mahoso like N'%"+hoSoSearchDTO.getMaHoSo()+"%'";
                        queryCount += " AND hs.mahoso like N'%"+hoSoSearchDTO.getMaHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getFromDate() != null) {
                    query += " and hs.ngaytiepnhan >= '" + formatter.format(hoSoSearchDTO.getFromDate()) +"'";
                    queryCount += " and hs.ngaytiepnhan >= '" + formatter.format(hoSoSearchDTO.getFromDate()) +"'";
                }

                if(hoSoSearchDTO.getToDate() != null) {
                    query += " and hs.ngaytiepnhan <= '" + formatter.format(hoSoSearchDTO.getToDate()) +"'";
                    queryCount += " and hs.ngaytiepnhan <= '" + formatter.format(hoSoSearchDTO.getToDate()) +"'";
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND hs.appid = "+hoSoSearchDTO.getAppID();
                    queryCount += " AND hs.appid = "+hoSoSearchDTO.getAppID();
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND  hs.appid = "+hoSoSearchDTO.getAppID();
                    queryCount += " AND  hs.appid = "+hoSoSearchDTO.getAppID();
                }
            }
            query += " ORDER BY hs.ngaytiepnhan DESC OFFSET "+pageable.getOffset()+" ROWS FETCH NEXT "+pageable.getPageSize()+" ROWS ONLY";

            List<HoSoResponse> hoSoList = null;
            List<HoSoResponse> hss = new ArrayList<>(entityManager.createNativeQuery(query, HoSoResponse.class).getResultList());
            hoSoList = new ArrayList<HoSoResponse>(hss);
            hoSoList.sort((a, b) -> {
                return b.getNgayTiepNhan().compareTo(a.getNgayTiepNhan());
            });

            // Query count total record

            Integer totalRecord = (Integer) entityManager.createNativeQuery(queryCount).getSingleResult();

            if (hoSoList != null) {
                hoSoPage = new PageImpl<HoSoResponse>(hoSoList, pageable,
                        totalRecord);
            }
            return hoSoPage;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        finally {
            entityManager.close();
        }
        return hoSoPage;
    }


    @Override
    public List<HoSoResponse> getList ( HoSoSearchDTO hoSoSearchDTO ) {
        List<HoSoResponse> hoSoPage = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String query = "SELECT DISTINCT hs.id, hs.mahoso, hs.matthc, hs.kenhthuchien, hs.tendoanhnghiep, hs.chuhoso, hs.email, hs.fax, hs.sodienthoai, hs.ngaytiepnhan, hs.ngayhentra, aptt.tentrangthai,\n" +
                    "hs.ngaytra, hs.hinhthuc, hs.ngayketthucxuly, ap.tenapp, hs.rawjson, hs.documenttype, hs.manoibo, hs.madoanhnghiep, hs.macoquancapdangky, hs.loaihinhdoanhnghiep, hs.trangthaihoso\n" +
                    "FROM hoSo hs \n" +
                    "  INNER JOIN Application ap on hs.AppID=ap.ID\n" +
                    "  INNER JOIN ApplicationTT aptt on hs.AppID=aptt.AppID\n" +
                    "WHERE (hs.fax != '"+deleteValue+"' or hs.fax is null) and ( hs.TrangThaiHoSo = aptt.IDTrangThaiDVC or hs.TrangThaiHoSo is null ) and hs.ngaytiepnhan is not null ";

            if (hoSoSearchDTO != null) {
                if(hoSoSearchDTO.getMaTTHC() != null) {
                    if(!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        query += " AND hs.matthc = '"+hoSoSearchDTO.getMaTTHC()+"'";
                    }
                }

                if(hoSoSearchDTO.getChuHoSo() != null) {
                    if(!hoSoSearchDTO.getChuHoSo().isEmpty()) {
                        query += " AND hs.chuhoso like '%"+hoSoSearchDTO.getChuHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getMaHoSo() != null) {
                    if(!hoSoSearchDTO.getMaHoSo().isEmpty()) {
                        query += " AND hs.mahoso like '%"+hoSoSearchDTO.getMaHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getFromDate() != null) {
                    query += " and hs.ngaytiepnhan >= '" + formatter.format(hoSoSearchDTO.getFromDate()) +"'";
                }

                if(hoSoSearchDTO.getToDate() != null) {
                    query += " and hs.ngaytiepnhan <= '" + formatter.format(hoSoSearchDTO.getToDate()) +"'";
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND hs.appid = "+hoSoSearchDTO.getAppID();
                }

//                if(hoSoSearchDTO.getAppID() != null ) {
//                    query += " AND  hs.appid = "+hoSoSearchDTO.getAppID();
//                }
            }
            query += " ORDER BY hs.ngaytiepnhan DESC";

            List<HoSoResponse> hoSoList = null;
            Set<HoSoResponse> hss = new HashSet<HoSoResponse>(entityManager.createNativeQuery(query, HoSoResponse.class).getResultList());
            hoSoList = new ArrayList<HoSoResponse>(hss);
            hoSoList.sort((a, b) -> {
                return b.getNgayTiepNhan().compareTo(a.getNgayTiepNhan());
            });
            return hoSoList;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        finally {
            entityManager.close();
        }
        return hoSoPage;
    }

    @Override
    public Page<HoSo> getPage(Pageable pageable, String filter) {
        Page<HoSo> hoSos = null;
        try {
            hoSos = hoSoRepository.findAll(HoSoSpecification.filterSearch(filter), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hoSos;
    }

    @Override
    public List<HoSo> getList() {
        List<HoSo> hoSos = null;
        try {
            hoSos = hoSoRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hoSos;
    }


    @Override
    public HoSo getOne(int id) {
        HoSo hoSo = null;
        try {
            hoSo = hoSoRepository.findById(id);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hoSo;
    }

    @Override
    public Page<CountStatusProfileDTO> countStatusProfile ( HoSoSearchDTO hoSoSearchDTO, Pageable pageable ) {
        Page<CountStatusProfileDTO> statusProfileDTOPage = null ;
        try {
            StringBuilder mainQuery = new StringBuilder( "SELECT table_tong.ngay, table_dangxuly.dangxuly, table_dacokq.dacokq, table_huy.dahuy\n" +
                    "FROM \n" +
                    "( select CONVERT(DATE,NgayTiepNhan) as ngay\n" +
                    "\tfrom hoSo\n" +
                    "\tWhere NgayTiepNhan IS NOT NULL \n" +
                    "\tGROUP BY CONVERT(DATE,NgayTiepNhan)\n" +
                    ") AS table_tong\n" +
                    "FULL JOIN\n" +
                    "(");
            StringBuilder queryDangXuLy = new StringBuilder(" select CONVERT(DATE,hs.NgayTiepNhan) as ngay, COUNT( DISTINCT hs.id) as dangxuly\n" +
                    "\tfrom hoSo as hs , (\n" +
                    "\t\tSELECT apptt.IDTrangThaiDVC, apptt.appid\n" +
                    "\t\tFROM ApplicationTT as apptt INNER JOIN DMTTMapping as ttmap ON apptt.id = ToTTID\n" +
                    "\t\tWHERE ttmap.FromTTID = "+this.dangXuLyInt+" \n" +
                    "\t) as apptt\n" +
                    "\tWHERE hs.TrangThaiHoSo = apptt.IDTrangThaiDVC and apptt.appid = hs.appid  AND hs.NgayTiepNhan IS NOT NULL AND (hs.fax != '"+deleteValue+"' or hs.fax is null)");

            StringBuilder queryDaCoKetQua = new StringBuilder(" select CONVERT(DATE,hs.NgayTiepNhan) as ngay, COUNT( DISTINCT hs.id) as dacokq\n" +
                    "\tfrom hoSo as hs , (\n" +
                    "\t\tSELECT apptt.IDTrangThaiDVC, apptt.appid\n" +
                    "\t\tFROM ApplicationTT as apptt INNER JOIN DMTTMapping as ttmap ON apptt.id = ToTTID\n" +
                    "\t\tWHERE ttmap.FromTTID = "+this.daCoKQInt+" \n" +
                    "\t) as apptt\n" +
                    "\tWHERE hs.TrangThaiHoSo = apptt.IDTrangThaiDVC and apptt.appid = hs.appid  AND hs.NgayTiepNhan IS NOT NULL AND (hs.fax != '"+deleteValue+"' or hs.fax is null)");
            StringBuilder queryHuy = new StringBuilder(" select CONVERT(DATE,hs.NgayTiepNhan) as ngay, COUNT( DISTINCT hs.id) as dahuy\n" +
                    "\tfrom hoSo as hs , (\n" +
                    "\t\tSELECT apptt.IDTrangThaiDVC, apptt.appid\n" +
                    "\t\tFROM ApplicationTT as apptt INNER JOIN DMTTMapping as ttmap ON apptt.id = ToTTID\n" +
                    "\t\tWHERE ttmap.FromTTID = "+this.huyInt+" \n" +
                    "\t) as apptt\n" +
                    "\tWHERE hs.TrangThaiHoSo = apptt.IDTrangThaiDVC and apptt.appid = hs.appid  AND hs.NgayTiepNhan IS NOT NULL AND (hs.fax != '"+deleteValue+"' or hs.fax is null)");
            if(hoSoSearchDTO != null) {
                if (hoSoSearchDTO.getAppID() != null) {
                    queryDangXuLy.append(" AND hs.appid = " + hoSoSearchDTO.getAppID());
                    queryDaCoKetQua.append(" AND hs.appid = " + hoSoSearchDTO.getAppID());
                    queryHuy.append(" AND hs.appid = " + hoSoSearchDTO.getAppID());
                }
                if (hoSoSearchDTO.getMaTTHC() != null) {
                    if (!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        queryDangXuLy.append(" AND hs.matthc like %" + hoSoSearchDTO.getAppID() + "%");
                        queryDaCoKetQua.append(" AND hs.matthc like %" + hoSoSearchDTO.getAppID() + "%");
                        queryHuy.append(" AND hs.matthc like '%" + hoSoSearchDTO.getAppID() + "%'");
                    }
                }
            }
            queryDaCoKetQua.append(" GROUP BY CONVERT(DATE,NgayTiepNhan) ");
            queryDangXuLy.append(" GROUP BY CONVERT(DATE,NgayTiepNhan) ");
            queryHuy.append(" GROUP BY CONVERT(DATE,NgayTiepNhan) ");
            mainQuery.append(queryDangXuLy + " ) AS table_dangxuly ON table_tong.ngay = table_dangxuly.ngay\n" +
                    "FULL JOIN\n" +
                    "( " + queryDaCoKetQua + " ) AS table_dacokq ON table_tong.ngay = table_dacokq.ngay\n" +
                    "FULL JOIN\n" +
                    "( " + queryHuy + " ) AS table_huy ON table_tong.ngay = table_huy.ngay ");
            if(hoSoSearchDTO != null) {
                if (hoSoSearchDTO.getFromDate() != null || hoSoSearchDTO.getToDate() != null) {
                    mainQuery.append(" WHERE 1 = 1 ");
                    if (hoSoSearchDTO.getFromDate() != null) {
                        mainQuery.append("  AND table_tong.ngay >= '" + formatter.format(hoSoSearchDTO.getFromDate()) + "'");
                    }
                    if (hoSoSearchDTO.getToDate() != null) {
                        mainQuery.append(" AND table_tong.ngay <= '" + formatter.format(hoSoSearchDTO.getToDate()) + "'");
                    }
                }
            }
            mainQuery.append(" ORDER BY table_tong.ngay DESC");
            List<CountStatusProfileDTO> statusProfileDTOList = entityManager.createNativeQuery(mainQuery.toString(), CountStatusProfileDTO.class).getResultList();

            for(int i = 0; i < statusProfileDTOList.size(); i++) {
                if(statusProfileDTOList.get(i).getDacokq() == null && statusProfileDTOList.get(i).getDangxuly() == null
                        && statusProfileDTOList.get(i).getDahuy() == null ) {
                    statusProfileDTOList.remove(i);
                    --i;
                }
            }

            if (statusProfileDTOList != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > statusProfileDTOList.size()
                        ? statusProfileDTOList.size() : (start + pageable.getPageSize());
                statusProfileDTOPage = new PageImpl<CountStatusProfileDTO>(statusProfileDTOList.subList(start, end), pageable,
                        statusProfileDTOList.size());
            }
            return statusProfileDTOPage;
        } catch (Exception ex ) {
            ex.printStackTrace();
            return  statusProfileDTOPage;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public Page<HoSoResponse> getPageGetLogFollowDate ( HoSoSearchDTO hoSoSearchDTO, Pageable pageable, String type ) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Page<HoSoResponse> hoSoPage = null;
            String query = "select hs.id, hs.mahoso, hs.matthc, hs.kenhthuchien, hs.tendoanhnghiep, hs.chuhoso, hs.email, hs.fax, hs.sodienthoai, hs.ngaytiepnhan, hs.ngayhentra, apptt.tentrangthai,\n" +
                    "hs.ngaytra, hs.hinhthuc, hs.ngayketthucxuly, app.tenapp, hs.rawjson, hs.documenttype, hs.manoibo, hs.madoanhnghiep, hs.macoquancapdangky, hs.loaihinhdoanhnghiep, hs.trangthaihoso\n" +
                    "from hoSo as hs INNER JOIN Application app on hs.appid = app.id , (\n" +
                    "\t\tSELECT apptt.IDTrangThaiDVC, apptt.appid, apptt.tentrangthai\n" +
                    "\t\tFROM ApplicationTT as apptt INNER JOIN DMTTMapping as ttmap ON apptt.id = ToTTID\n" +
                    "\t\tWHERE ttmap.FromTTID = ";

            switch (type) {
                case "dangxuly": {
                    query += this.dangXuLyInt;
                    break;
                }
                case "dacokq": {
                    query += this.daCoKQInt;
                    break;
                }
                case "dahuy": {
                    query += this.huyInt;
                    break;
                }
                default: {
                    query += -1;
                }
            }
            query += "\n ) as apptt\n" +
                    "WHERE hs.TrangThaiHoSo = apptt.IDTrangThaiDVC and apptt.appid = hs.appid  AND hs.NgayTiepNhan IS NOT NULL AND (hs.fax != '"+deleteValue+"' or hs.fax is null) ";
            if (hoSoSearchDTO != null) {
                if(hoSoSearchDTO.getChooseDate() != null) {
                    Date date = (Date)formatter.parse(hoSoSearchDTO.getChooseDate());
                    query += " AND hs.NgayTiepNhan = '" + formatter.format(date)+"'";
                }
                if(hoSoSearchDTO.getMaTTHC() != null) {
                    if(!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        query += " AND hs.mathhc like '%"+hoSoSearchDTO.getMaTTHC()+"%'";
                    }
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND  hs.appid = "+hoSoSearchDTO.getAppID();
                }
            }
            query += " ORDER BY CONVERT(DATE,NgayTiepNhan) DESC";

            List<HoSoResponse> hoSoList = entityManager.createNativeQuery(query, HoSoResponse.class).getResultList();
            if (hoSoList != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > hoSoList.size()
                        ? hoSoList.size() : (start + pageable.getPageSize());
                hoSoPage = new PageImpl<HoSoResponse>(hoSoList.subList(start, end), pageable,
                        hoSoList.size());
            }

            return hoSoPage;
        } catch (Exception ex ) {
            ex.printStackTrace();
            return null;
        }
        finally {
            entityManager.close();
        }
    }

    public Page<StatusProfileHistory> findStatussHistoryById( Integer id, Pageable pageable ) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Page<StatusProfileHistory> hoSoPage = null;
            String query = "SELECT DISTINCT hs.id,hs.AppID, hs.MaHoSo,sp.ma_trang_thai, sp.ten_trang_thai as tentrangthaigui, sp.ngay_cap_phieu, sp.ngay_tra, apptt.IDTrangThaiDVC as matrangthai, dmttdvc.TenTrangThai\n" +
                    "FROM status_profile as sp INNER JOIN hoSo as hs ON sp.hoso_id = hs.ID\n" +
                    "\t\t\tINNER JOIN ApplicationTT as apptt on apptt.AppID = hs.AppID \n" +
                    "\t\t\tLEFT JOIN DMTrangThaiDVC dmttdvc on dmttdvc.ID = apptt.IDTrangThaiDVC \n" +
                    "WHERE (apptt.IDTrangThaiDVC = sp.ma_trang_thai or apptt.IDTrangThaiDVC = sp.ma_trang_thai_send) ";
            if (id != null) {
                query += " and hs.id = "+id;
            }
            query += " ORDER BY sp.ngay_cap_phieu ASC ";

            List<StatusProfileHistory> hoSoList = entityManager.createNativeQuery(query, StatusProfileHistory.class).getResultList();
            if (hoSoList != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > hoSoList.size()
                        ? hoSoList.size() : (start + pageable.getPageSize());
                hoSoPage = new PageImpl<StatusProfileHistory>(hoSoList.subList(start, end), pageable,
                        hoSoList.size());
            }

            return hoSoPage;
        } catch (Exception ex ) {
            ex.printStackTrace();
            return null;
        }
        finally {
            entityManager.close();
        }
    }

    public Page<StatusProfileHistory> findStatussHistoryNotDVCById( Integer id, Pageable pageable ) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Page<StatusProfileHistory> hoSoPage = null;
            String query = "SELECT DISTINCT hs.id,hs.AppID, hs.MaHoSo,sp.ma_trang_thai, sp.ten_trang_thai as tentrangthaigui, sp.ngay_cap_phieu, sp.ngay_tra, apptt.MaTrangThai, apptt.TenTrangThai\n" +
                    "FROM status_profile as sp INNER JOIN hoSo as hs ON sp.hoso_id = hs.ID\n" +
                    "\t\t\tINNER JOIN ApplicationTT as apptt on apptt.AppID = hs.AppID \n" +
                    "WHERE (apptt.MaTrangThai = sp.ma_trang_thai or apptt.MaTrangThai = sp.ma_trang_thai_send) ";
            if (id != null) {
                query += " and hs.id = "+id;
            }
            query += " ORDER BY sp.ngay_cap_phieu ASC ";

            List<StatusProfileHistory> hoSoList = entityManager.createNativeQuery(query, StatusProfileHistory.class).getResultList();
            if (hoSoList != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > hoSoList.size()
                        ? hoSoList.size() : (start + pageable.getPageSize());
                hoSoPage = new PageImpl<StatusProfileHistory>(hoSoList.subList(start, end), pageable,
                        hoSoList.size());
            }

            return hoSoPage;
        } catch (Exception ex ) {
            ex.printStackTrace();
            return null;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public Page<HoSoResponse> getPageHoTichAndLyLich(Pageable pageable, HoSoSearchDTO hoSoSearchDTO) {
        Page<HoSoResponse> hoSoPage = null;
        try {
            String query = "SELECT DISTINCT hs.id, hs.mahoso, hs.matthc, hs.kenhthuchien, hs.tendoanhnghiep, hs.chuhoso, hs.email, hs.fax, hs.sodienthoai, hs.ngaytiepnhan, hs.ngayhentra, hs.trangthaihoso as tentrangthai,\n" +
                    "hs.ngaytra, hs.hinhthuc, hs.ngayketthucxuly, ap.tenapp, hs.rawjson, hs.documenttype, hs.manoibo, hs.madoanhnghiep, hs.macoquancapdangky, hs.loaihinhdoanhnghiep, hs.trangthaihoso\n" +
                    "FROM hoSo hs \n" +
                    "  INNER JOIN Application ap on hs.AppID=ap.ID\n" +
                    "WHERE (hs.fax != '"+deleteValue+"' or hs.fax is null) ";

            if (hoSoSearchDTO != null) {
                if(hoSoSearchDTO.getMaTTHC() != null) {
                    if(!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        query += " AND hs.matthc = '"+hoSoSearchDTO.getMaTTHC()+"'";
                    }
                }

                if(hoSoSearchDTO.getChuHoSo() != null) {
                    if(!hoSoSearchDTO.getChuHoSo().isEmpty()) {
                        query += " AND hs.chuhoso like '%"+hoSoSearchDTO.getChuHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getMaHoSo() != null) {
                    if(!hoSoSearchDTO.getMaHoSo().isEmpty()) {
                        query += " AND hs.mahoso like '%"+hoSoSearchDTO.getMaHoSo()+"%'";
                    }
                }

                if(hoSoSearchDTO.getFromDate() != null) {
                    query += " and hs.ngaytiepnhan >= '" + formatter.format(hoSoSearchDTO.getFromDate()) +"'";
                }

                if(hoSoSearchDTO.getToDate() != null) {
                    query += " and hs.ngaytiepnhan <= '" + formatter.format(hoSoSearchDTO.getToDate()) +"'";
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND hs.appid = "+hoSoSearchDTO.getAppID();
                }

                if(hoSoSearchDTO.getAppID() != null ) {
                    query += " AND  hs.appid = "+hoSoSearchDTO.getAppID();
                }
            }
            query += " ORDER BY hs.ngaytiepnhan DESC";

            List<HoSoResponse> hoSoList = null;
            Set<HoSoResponse> hss = new HashSet<HoSoResponse>(entityManager.createNativeQuery(query, HoSoResponse.class).getResultList());
            hoSoList = new ArrayList<HoSoResponse>(hss);
            hoSoList.sort((a, b) -> {
                return b.getNgayTiepNhan().compareTo(a.getNgayTiepNhan());
            });
            if (hoSoList != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > hoSoList.size()
                        ? hoSoList.size() : (start + pageable.getPageSize());
                hoSoPage = new PageImpl<HoSoResponse>(hoSoList.subList(start, end), pageable,
                        hoSoList.size());
            }
            return hoSoPage;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        finally {
            entityManager.close();
        }
        return hoSoPage;
    }

    @Override
    public StatusMappingDTO getStatusDetail ( Integer appid, Integer trangThaiTrenBo ) {
        try {
            StatusMappingDTO statusMappingDTO = new StatusMappingDTO();
            String query = "Select apptt.appid, apptt.matrangthai, apptt.tentrangthai, apptt.IDTrangThaiDVC, dvctt.TenTrangThai as tentrangthaiDVC, dvctt.ghichu, dvctt.status\n" +
                    "FROM ApplicationTT apptt INNER JOIN DMTrangThaiDVC dvctt on apptt.IDTrangThaiDVC = dvctt.ID WHERE  1=1 ";
            if (appid != null) {
                query += " and apptt.AppID = "+appid;
            }

            if (trangThaiTrenBo != null) {
                query += " and apptt.MaTrangThai = "+trangThaiTrenBo;
            }

            statusMappingDTO = (StatusMappingDTO) entityManager.createNativeQuery(query, StatusMappingDTO.class).getSingleResult();
            return statusMappingDTO;
        } catch (Exception ex ) {
            ex.printStackTrace();
            return null;
        }
        finally {
            entityManager.close();
        }
    }

}
