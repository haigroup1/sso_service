package com.savis.categories.service.impl;

import com.savis.categories.common.util.DateUtils;
import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import com.savis.categories.service.LogProfileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class LogProfileServiceImpl implements LogProfileService {

    @Value("${elasticsearch.host}")
    private String esHost;

    @Value("${elasticsearch.port}")
    private int esPort;

    @Override
    public Page<Object> getLog ( HoSoSearchDTO searchForm, Pageable pageable ) {
        List<Object> pay = null;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> response = null;
        String esLink = "http://" + this.esHost + ":" + this.esPort;

        boolean iscomman = false;

        String url = esLink + "/lgsphaiduong-*/_search";
        String url_count = esLink + "/lgsphaiduong-*/_count";

        String jsonSearch = "{\n" +
                "  \"query\": {\n" +
                "    \"bool\": {\n" +
                "      \"must\": [";

        if (StringUtils.isNotBlank(searchForm.getApiPath())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"api_path\":\"" + searchForm.getApiPath() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getAppIdElastic())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"appId\":\"" + searchForm.getAppIdElastic() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getIdReceivedDec())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"request_header_json.idReceivedDec\":\"" + searchForm.getIdReceivedDec() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getIdMoneyReceipt())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"request_header_json.idMoneyReceipt\":\"" + searchForm.getIdMoneyReceipt() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getInfoType())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            if(iscomman) jsonSearch += ",";
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"request_header_json.infoType\":\"" + searchForm.getInfoType() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getDecStatusId())) {
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"response_body_json.listContentJson.decStatusId\":\"" + searchForm.getDecStatusId() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getMaTTHC())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"response_body_json.listContentJson.declareTypeId\":\"" + searchForm.getMaTTHC() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getStatus())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"response_body_json.status\":\"" + searchForm.getStatus() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        if (StringUtils.isNotBlank(searchForm.getDescription())) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"match\":{\n" +
                    "                  \"response_body_json.description\":\"" + searchForm.getDescription() + "\"\n" +
                    "               }\n" +
                    "            }\n";
        }

        /*if (StringUtils.isNotBlank(searchForm.getType_log())) {
            jsonSearch += "            {\n" +
                    "                \"regexp\": {\n" +
                    "                    \"action\": \"(out|in)\"\n" +
                    "                }\n" +
                    "            }";
        }*/

        jsonSearch += "      ]\n";

        //filter date range
        iscomman = false;
        jsonSearch += "         ,\"filter\":[\n";
        if(searchForm.getFormDate() != null || searchForm.getToDate() != null ){
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"range\":{\n" +
                    "                  \"log_date\":{\n";
            if(searchForm.getFormDate() != null) {
                jsonSearch += "                     \"gte\":\"" + DateUtils.date2str(searchForm.getFormDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            if(searchForm.getToDate() != null) {
                jsonSearch += "                     \"lte\":\"" + DateUtils.date2str(searchForm.getToDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            jsonSearch += "                     \"format\":\"dd-MM-yyyy HH:mm:ss\"\n" +
                    "                  }\n" +
                    "               }\n" +
                    "            }\n";
        }
        if(searchForm.getFromDate() != null || searchForm.getToDate() != null ) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"range\":{\n" +
                    "                  \"request_header_json.dateReceivedDec\":{\n";
            if(searchForm.getFromDate() != null) {
                jsonSearch += "                     \"gte\":\"" + DateUtils.date2str(searchForm.getFromDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            if(searchForm.getToDate() != null) {
                jsonSearch += "                     \"lte\":\"" + DateUtils.date2str(searchForm.getToDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            jsonSearch += "                     \"format\":\"dd-MM-yyyy HH:mm:ss\"\n" +
                    "                  }\n" +
                    "               }\n" +
                    "            }\n";
        }
        if(searchForm.getFromLogDate() != null || searchForm.getToLogDate() != null ) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"range\":{\n" +
                    "                  \"log_date\":{\n";
            if(searchForm.getFromLogDate() != null) {
                jsonSearch += "                     \"gte\":\"" + DateUtils.date2str(searchForm.getFromLogDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            if(searchForm.getToLogDate() != null) {
                jsonSearch += "                     \"lte\":\"" + DateUtils.date2str(searchForm.getToLogDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            jsonSearch += "                     \"format\":\"dd-MM-yyyy HH:mm:ss\"\n" +
                    "                  }\n" +
                    "               }\n" +
                    "            }\n";
        }
        if(searchForm.getDatePromissoryDecFrom() != null || searchForm.getDatePromissoryDecTo() != null ) {
            if(iscomman) jsonSearch += ",";
            iscomman = true;
            jsonSearch += "            {\n" +
                    "               \"range\":{\n" +
                    "                  \"request_header_json.datePromissoryDec\":{\n";
            if(searchForm.getDatePromissoryDecFrom() != null) {
                jsonSearch += "                     \"gte\":\"" + DateUtils.date2str(searchForm.getDatePromissoryDecFrom(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            if(searchForm.getDatePromissoryDecTo() != null) {
                jsonSearch += "                     \"lte\":\"" + DateUtils.date2str(searchForm.getDatePromissoryDecTo(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
            }
            jsonSearch += "                     \"format\":\"dd-MM-yyyy HH:mm:ss\"\n" +
                    "                  }\n" +
                    "               }\n" +
                    "            }\n";
        }
        jsonSearch += "         ]\n";

        //phân trang + sort
        String jsonCount = "    }\n" +
                "  },\n" +
                "  \"from\": " + pageable.getOffset() + ",\n" +
                "  \"size\": " + pageable.getPageSize() + ",\n" +
                "    \"sort\": [\n" +
                "        { \"@timestamp\": { \"order\": \"desc\" }}\n" +
                "    ]\n";
        String jsonEnd = "}";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> request = new HttpEntity<>(jsonSearch + jsonCount + jsonEnd, headers);
        response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);

        LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();
        LinkedHashMap<String, Object> mapHits = (LinkedHashMap<String, Object>) map.get("hits");
        List<Object> mapHit = (List<Object>) mapHits.get("hits");

        request = new HttpEntity<>(jsonSearch + jsonEnd + jsonEnd + jsonEnd, headers);
        response = restTemplate.exchange(url_count, HttpMethod.POST, request, Object.class);
        map = (LinkedHashMap<String, Object>) response.getBody();
        String count = map.get("count").toString();

        return new PageImpl<>(mapHit, pageable, Long.parseLong(count));
    }

    public boolean deleteById(String _index, String _id) {
        try {
            String esLink = "http://" + this.esHost + ":" + this.esPort + "/" + _index + "/doc/" + _id;
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.exchange(esLink, HttpMethod.DELETE, new HttpEntity<>(new HttpHeaders()), Object.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Object getById ( String _index, String _id, String _type ) {
        Object object = null;
        try {
            String esLink = "http://" + this.esHost + ":" + this.esPort + "/" + _index + "/" + _type + "/" + _id;
            RestTemplate restTemplate = new RestTemplate();
            object = restTemplate.exchange(esLink, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), Object.class);
        } catch (Exception e) {
            e.printStackTrace();
            return object;
        }
        return object;
    }

}
