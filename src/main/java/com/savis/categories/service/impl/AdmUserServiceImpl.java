package com.savis.categories.service.impl;

import com.savis.categories.auth.dto.adm.user.AdmUserDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.common.util.CommonUtils;
import com.savis.categories.dao.entity.adm.AdmRight;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.AdmUser;
import com.savis.categories.dao.entity.adm.AdmUserRole;
import com.savis.categories.dao.repository.AdmRoleRepository;
import com.savis.categories.dao.repository.AdmUserRepository;
import com.savis.categories.dao.repository.AdmUserRoleRepository;
import com.savis.categories.dao.specifications.AdmUserSpecification;
import com.savis.categories.service.AdmUserService;
import com.savis.categories.service.UserAdService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@PropertySource("classpath:application.yml")
public class AdmUserServiceImpl implements AdmUserService {

	@Autowired
	private AdmUserRepository admUserRepository;

	@Autowired
	private AdmRoleRepository admRoleRepository;

	@Autowired
	private AdmUserRoleRepository admUserRoleRepository;

	@Autowired
	private UserAdService userAdService;

	@Value("${ad.baseOu}")
	private String baseOu = "";// ex: ou=soft,dc=esb,dc=lab

	@Value("${ad.passDefault}")
	private String passDefault = "";// ex: ou=soft,dc=esb,dc=lab

	private Logger log = Logger.getLogger(AdmUserServiceImpl.class);

	@Override
	public Page<AdmUser> getPage( Pageable pageable) {
		Page<AdmUser> admUserPage = null;
		try {
			admUserPage = admUserRepository.findAll(pageable);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUserPage;
	}

	@Override
	public Page<AdmUser> getPage(Pageable pageable, AdmUser admUser) {
		Page<AdmUser> admUserPage = null;
		try {
			admUserPage = admUserRepository.findAll(AdmUserSpecification.advanceSearch(admUser), pageable);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUserPage;
	}

	@Override
	public Page<AdmUser> getPage(Pageable pageable, String filter) {
		Page<AdmUser> admUserPage = null;
		try {
			admUserPage = admUserRepository.findAll(AdmUserSpecification.filterSearch(filter), pageable);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUserPage;
	}

	@Override
	public List<AdmUser> getList() {
		List<AdmUser> admUserList = null;
		try {
			admUserList = admUserRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUserList;
	}

	@Override
	public List<AdmUser> getList(int applicationId) {
		List<AdmUser> admUserList = null;
		try {
			admUserList = admUserRepository.findByApplicationId(applicationId);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUserList;
	}

	@Override
	public AdmUser getOne(int id) {
		AdmUser admUser = null;
		try {
			admUser = admUserRepository.findByUserId(id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return admUser;
	}

	@Transactional
	@Override
	public Result create( AdmUserDTO admUserDTO) {
		try {
			int checkCreate = 1;
//			try {
//				checkCreate = userAdService.createNewUser(admUserDTO.getUserName(), admUserDTO.getSurname(),
//						admUserDTO.getGivenName(), baseOu);
//			} catch (Exception e) {
//				// TODO: handle exception
//				checkCreate = 1;
//			}

			int checkCreatePass = 1;

//			try {
//				checkCreatePass = userAdService.updateUserPassword(admUserDTO.getUserName(), passDefault, baseOu);
//			} catch (Exception e) {
//				// TODO: handle exception
//				checkCreatePass = 1;
//			}

			if (checkCreate == 1 && checkCreatePass == 1) {
				AdmUser admUser = admUserRepository.save(this.toEntity(admUserDTO));
				// set role
				Integer [] roldIds = admUserDTO.getRoleIds();
				if (roldIds != null && roldIds.length > 0) {
					for (int i = 0; i < roldIds.length; i++) {
						AdmUserRole admUserRole = new AdmUserRole();
						admUserRole.setAdmUser(admUser.getUserId());
						admUserRole.setAdmRole(admRoleRepository.findByRoleId(roldIds[i]).getRoleId());
						admUserRole.setCreator(CommonUtils.getCurrentUserName());
						admUserRole.setCreated(CommonUtils.getCurrentDate());

						admUserRoleRepository.save(admUserRole);
					}
				}
				return Result.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return Result.BAD_REQUEST;
	}

	@Override
	public Result update( AdmUserDTO admUserDTO) {
		try {
			AdmUser admUserExit = admUserRepository.findByUserId(admUserDTO.getUserId());
			if (admUserExit != null) {
				List<AdmUserRole> admUserRolesExit = admUserRoleRepository.findByAdmUser(admUserExit.getUserId());
				AdmUser newAdmUser = toEntity(admUserDTO);

				// remove role in user
				for(AdmUserRole temp : admUserRolesExit) {
					admUserRoleRepository.deleteById(temp.getId());
				}

				AdmUser admUserUpdated = admUserRepository.save(newAdmUser);
				// set role
				if (admUserDTO.getRoleIds() != null) {
					if(admUserDTO.getRoleIds().length > 0 ) {
						Integer[] roldIds = admUserDTO.getRoleIds();
						for (int i = 0; i < roldIds.length; i++) {
							AdmUserRole admUserRole = new AdmUserRole();
							admUserRole.setAdmUser(admUserUpdated.getUserId());
							admUserRole.setAdmRole(admRoleRepository.findByRoleId(roldIds[i]).getRoleId());
							admUserRole.setCreator(CommonUtils.getCurrentUserName());
							admUserRole.setCreated(CommonUtils.getCurrentDate());

							saveMethod(admUserRole);
						}
					}
				}

				return Result.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return Result.BAD_REQUEST;
	}

	@Transactional
	@Modifying
	void saveMethod ( AdmUserRole admUserRole ) {
		try {
			admUserRole.setId(null);
			AdmUserRole test = admUserRoleRepository.saveAndFlush(admUserRole);
			System.out.println(test);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Result delete( int id) {
		try {
			admUserRepository.delete(id);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return Result.BAD_REQUEST;
	}

	@Override
	@Transactional
	@Modifying
	public Result delete(int[] ids) {
		try {
			List<AdmUser> lAdmUsers = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) {
				lAdmUsers.add(admUserRepository.findByUserId(ids[i]));
			}
			admUserRepository.delete(lAdmUsers);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return Result.BAD_REQUEST;
	}

//	@Override
//	public List<AdmUserRole> getUserRoleByUser(int userId) {
//		List<AdmUserRole> amAdmUserRoles = null;
//		try {
//			AdmUser admUser = admUserRepository.findByUserId(userId);
//			amAdmUserRoles = admUserRoleRepository.findByAdmUser(admUser);
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error(e);
//		}
//		return amAdmUserRoles;
//	}

	private List<AdmUserRole> getListUserRoleRemove(List<AdmUserRole> listUserRoleOld,
			List<AdmUserRole> listUserRoleNew) {
		List<AdmUserRole> listRemove = new ArrayList<>();
		for (AdmUserRole userRoleOld : listUserRoleOld) {
			boolean flat = true;
			for (AdmUserRole userRoleNew : listUserRoleNew) {
				if (userRoleOld.equals(userRoleNew))
					flat = false;
			}
			if (flat)
				listRemove.add(userRoleOld);
		}
		return listRemove;
	}
//
//	private List<AdmUserRole> getListUserRoleAdd(List<AdmUserRole> listUserRoleOld, List<AdmUserRole> listUserRoleNew) {
//		List<AdmUserRole> listAdds = new ArrayList<>();
//		for (AdmUserRole userRoleNew : listUserRoleNew) {
//			boolean flat = true;
//			for (AdmUserRole userRoleOld : listUserRoleOld) {
//				if (userRoleOld.equals(userRoleNew))
//					flat = false;
//			}
//			if (flat)
//				listAdds.add(userRoleNew);
//		}
//		return listAdds;
//	}

	private AdmUser toEntity(AdmUserDTO admUserDTO) {
		AdmUser admUser = new AdmUser();

		// set user infomation
		admUser.setUserId(admUserDTO.getUserId());
		admUser.setUserName(admUserDTO.getUserName());
		admUser.setLoweredUsername(admUserDTO.getLoweredUsername());
		admUser.setSurname(admUserDTO.getSurname());
		admUser.setGivenName(admUserDTO.getGivenName());
		admUser.setDisplayName(admUserDTO.getDisplayName());
		admUser.setMobileAlias(admUserDTO.getMobileAlias());
		admUser.setIsAnonymous(admUserDTO.getIsAnonymous());
		admUser.setIsDisable(admUserDTO.getIsDisable());
		admUser.setLastActivityDate(CommonUtils.getCurrentDate());
		admUser.setUserType(admUserDTO.getUserType());
		admUser.setApplicationId(admUserDTO.getApplicationId());

		return admUser;
	}

}
