package com.savis.categories.service.impl;

import com.savis.categories.dao.entity.DMThuTucHC;
import com.savis.categories.dao.repository.DMThuTucHCRepository;
import com.savis.categories.service.DMThuTucHCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import java.util.List;

@Service
public class DMThuTucHCServiceImpl implements DMThuTucHCService {

    @Autowired
    private DMThuTucHCRepository dmThuTucHCRespository;

    @Override
    public List<DMThuTucHC> listDMThuTucHC ( Integer appID ) {
        try {
            return dmThuTucHCRespository.findAllByAppID(appID);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<DMThuTucHC> listDMThuTucHCByApp ( Integer appID, Pageable pageable ) {
        try {
            return dmThuTucHCRespository.findAllByAppID(appID, pageable);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<DMThuTucHC> getPage ( Pageable pageable ) {
        try {
            return dmThuTucHCRespository.findAll(pageable);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public DMThuTucHC findByMaThuTuc ( String maThuTuc, Integer appID ) {
        try {
            return dmThuTucHCRespository.findByMaThuTucAndAndAppID(maThuTuc, appID);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
