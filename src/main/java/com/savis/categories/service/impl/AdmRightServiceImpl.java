package com.savis.categories.service.impl;

import com.savis.categories.auth.dto.adm.right.AdmRightDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRight;
import com.savis.categories.dao.repository.AdmRightRepository;
import com.savis.categories.dao.specifications.AdmRightSpecification;
import com.savis.categories.service.AdmRightService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdmRightServiceImpl implements AdmRightService {

    @Autowired
    private AdmRightRepository admRightRepository;


    private Logger log = Logger.getLogger(AdmRightServiceImpl.class);

    @Override
    public Page<AdmRight> getPage(Pageable pageable) {
        Page<AdmRight> admRightPage = null;
        try {
            admRightPage = admRightRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRightPage;
    }

    @Override
    public Page<AdmRight> getPage(Pageable pageable, AdmRight admRight) {
        Page<AdmRight> admRightPage = null;
        try {
            admRightPage = admRightRepository.findAll(AdmRightSpecification.advanceSearch(admRight), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRightPage;
    }

    @Override
    public Page<AdmRight> getPage(Pageable pageable, String filter) {
        Page<AdmRight> admRightPage = null;
        try {
            admRightPage = admRightRepository.findAll(AdmRightSpecification.filterSearch(filter), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRightPage;
    }

    @Override
    public List<AdmRight> getList() {
        List<AdmRight> admRightList = null;
        try {
            admRightList = admRightRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRightList;
    }

    @Override
    public List<AdmRight> getListParent() {
        List<AdmRight> admRightList = null;
        try {
            admRightList = admRightRepository.findByHasChild(1);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRightList;
    }


    @Override
    public AdmRight getOne(int id) {
        AdmRight admRight = null;
        try {
            admRight = admRightRepository.findByRightId(id);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return admRight;
    }

    @Override
    public Result create( AdmRightDTO admRightDTO) {
        try {
            admRightRepository.save(this.toEntity(admRightDTO));
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    @Override
    @Transactional
    public Result update(AdmRightDTO admRightDTO) {
        try {
            AdmRight admRightExist = admRightRepository.findByRightId(admRightDTO.getRightId());
            if (admRightExist != null) {
                AdmRight admRightNew = this.toEntity(admRightDTO);

                admRightRepository.save(admRightNew);
                return Result.SUCCESS;
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result delete(int id) {
        try {
            admRightRepository.delete(id);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    @Override
    @Transactional
    public Result delete(int[] ids) {
        try {
            List<AdmRight> lAdmRights = new ArrayList<>();
            for (int i = 0; i < ids.length; i++) {
                lAdmRights.add(admRightRepository.findByRightId(ids[i]));
            }
            admRightRepository.delete(lAdmRights);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    private AdmRight toEntity(AdmRightDTO admRightDTO) {
        AdmRight admRight = new AdmRight();
        admRight.setRightId(admRightDTO.getRightId());
        admRight.setParentRightId(admRightDTO.getParentRightId());
        admRight.setRightCode(admRightDTO.getRightCode());
        admRight.setRightName(admRightDTO.getRightName());
        admRight.setStatus(admRightDTO.getStatus());
        admRight.setRightOrder(admRightDTO.getRightOrder());
        admRight.setHasChild(admRightDTO.getHasChild());
        admRight.setUrlRewrite(admRightDTO.getUrlRewrite());
        admRight.setIconUrl(admRightDTO.getIconUrl());
        admRight.setDescription(admRightDTO.getDescription());

        // save access

        return admRight;
    }
}
