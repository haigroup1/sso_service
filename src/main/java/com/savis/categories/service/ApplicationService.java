package com.savis.categories.service;

import com.savis.categories.auth.dto.adm.application.ApplicationDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ApplicationService {

    public List<Application> getListApplication();

    public Page<Application> getPageAppliaction( Application application, Pageable pageable );

    public Application getOne(Integer appid);

    public Result create(ApplicationDTO application);

    public Result update(ApplicationDTO application);

    public Result delete(Integer appid);

    public Result delete ( int[] ids );

}
