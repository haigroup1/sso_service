package com.savis.categories.service;

import com.savis.categories.auth.dto.user.AccessTokenInfo;
import com.savis.categories.auth.dto.user.UserInfo;

public interface IsUserService {

	UserInfo getUserInfo(String authorizationCode);

	AccessTokenInfo getAccessToken(String refreshToken);

}
