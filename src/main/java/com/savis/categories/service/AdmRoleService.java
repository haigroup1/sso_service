package com.savis.categories.service;

import com.savis.categories.auth.dto.adm.role.AdmRoleDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdmRoleService {

	/**
	 * 
	 * @param pageable
	 * @return a {@link Page} of Role
	 */
	Page<AdmRole> getPage ( Pageable pageable );

	/**
	 *
	 * @param pageable
	 * @param admRole:
	 *            the search object
	 * @return a {@link Page} of Role
	 */
	Page<AdmRole> getPage ( Pageable pageable, AdmRole admRole );

	/**
	 *
	 * @param pageable
	 * @param filter:
	 *            the search keywords
	 * @return a {@link Page} of Role
	 */
	Page<AdmRole> getPage ( Pageable pageable, String filter );

	/**
	 *
	 * @return a {@link List} of Role
	 */
	List<AdmRole> getList ();

	/**
	 *
	 * @param ApplicationId:
	 *            the application id
	 * @return a {@link List} of Role
	 */
	List<AdmRole> getList ( int ApplicationId );

	/**
	 *
	 * @param id:
	 *            the id of the Role
	 * @return a {@link AdmRole}
	 */
	AdmRole getOne ( int id );


	Result create ( AdmRoleDTO admRoleDTO );


	Result update ( AdmRoleDTO admRoleDTO );

	/**
	 *
	 * @param id:
	 *            the id of the Role
	 * @return a {@link Result}
	 */
	Result delete ( int id );

	/**
	 * delete a list Role
	 *
	 * @param ids:
	 *            the list id
	 * @return a {@link Result}
	 */
	Result delete ( int[] ids );

}
