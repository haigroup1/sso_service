package com.savis.categories.service;


import com.savis.categories.dao.entity.dto.UserAD;

public interface UserAdService {

	/**
	 * Create a New User (Note no password is assigned)
	 * 
	 * @param username
	 * @param surname
	 * @param givenName
	 */
	public int createNewUser ( String username, String surname, String givenName, String ou );

	/**
	 * Simple utility to update String Attribute Values
	 *
	 * @param username
	 * @param attributeName
	 * @param attributeValue
	 */
	public int updateUser ( String username, String attributeName, String attributeValue, String ou );

	/**
	 * Update User Password in Microsoft Active Directory
	 *
	 * @param username
	 * @param password
	 */
	public int updateUserPassword ( String username, String password, String ou );

	/**
	 *
	 * @param username
	 * @return
	 */
	public UserAD fetchUserAttributes ( String username, String ou );

}
