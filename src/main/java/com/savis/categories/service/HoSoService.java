package com.savis.categories.service;

import java.util.List;

import com.savis.categories.dao.entity.HoSo;
import com.savis.categories.dao.entity.StatusProfileHistory;
import com.savis.categories.dao.entity.dto.CountStatusProfileDTO;
import com.savis.categories.dao.entity.dto.HoSoResponse;
import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import com.savis.categories.dao.entity.dto.StatusMappingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HoSoService {

    /**
     *
     * @param pageable
     * @return a {@link Page} of api
     */
    Page<HoSo> getPage(Pageable pageable);


    Page<HoSoResponse> getPage(Pageable pageable, HoSoSearchDTO hoSoSearchDTO);

    List<HoSoResponse> getList(HoSoSearchDTO hoSoSearchDTO);

    /**
     *
     * @param pageable
     * @param filter: the search keywords
     * @return a {@link Page} of api
     */
    Page<HoSo> getPage(Pageable pageable, String filter);

    /**
     *
     * @return a {@link List} of api
     */
    List<HoSo> getList();


    HoSo getOne(int id);

    public Page<CountStatusProfileDTO> countStatusProfile(HoSoSearchDTO hoSoSearchDTO, Pageable pageable);

    public Page<HoSoResponse> getPageGetLogFollowDate ( HoSoSearchDTO hoSoSearchDTO, Pageable pageable, String type);

    public Page<StatusProfileHistory> findStatussHistoryById( Integer id, Pageable pageable );

    public Page<StatusProfileHistory> findStatussHistoryNotDVCById( Integer id, Pageable pageable );

    public Page<HoSoResponse> getPageHoTichAndLyLich(Pageable pageable, HoSoSearchDTO hoSoSearchDTO);

    public StatusMappingDTO getStatusDetail(Integer appid, Integer trangThaiTrenBo);

}

