package com.savis.categories.service;

import com.savis.categories.auth.dto.adm.right.AdmRightDTO;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRight;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdmRightService {

	/**
	 * 
	 * @param pageable
	 * @return a {@link Page} of Right
	 */
	Page<AdmRight> getPage ( Pageable pageable );

	/**
	 *
	 * @param pageable
	 * @param admRight:
	 *            the search object
	 * @return a {@link Page} of Right
	 */
	Page<AdmRight> getPage ( Pageable pageable, AdmRight admRight );

	/**
	 *
	 * @param pageable
	 * @param filter:
	 *            the search keywords
	 * @return a {@link Page} of Right
	 */
	Page<AdmRight> getPage ( Pageable pageable, String filter );

	/**
	 *
	 * @return a {@link List} of Right
	 */
	List<AdmRight> getList ();

	/**
	 *
	 * @return a {@link List} of Right
	 */
	List<AdmRight> getListParent ();

	/**
	 *
	 * @param id:
	 *            the id of the Right
	 * @return a {@link AdmRight}
	 */
	AdmRight getOne ( int id );

	/**
	 * create a new Right
	 *
	 * @param admRightDTO:
	 *            the new Right
	 * @return a {@link Result}
	 */
	Result create ( AdmRightDTO admRightDTO );

	/**
	 * update infomation of the Right
	 *
	 * @param admRightDTO:
	 *            the new Right
	 * @return a {@link Result}
	 */
	Result update ( AdmRightDTO admRightDTO );

	/**
	 *
	 * @param id:
	 *            the id of the Right
	 * @return a {@link Result}
	 */
	Result delete ( int id );

	/**
	 * delete a list Right
	 *
	 * @param ids:
	 *            the list id
	 * @return a {@link Result}
	 */
	Result delete ( int[] ids );
//
//	List<AdmAccessRight> getAccessRightByRight ( int rightId );
//
//	List<AdmApiRight> getApiByRight ( int rightId );
}
