package com.savis.categories.service;

import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LogProfileService {
    public Page<Object> getLog ( HoSoSearchDTO searchForm, Pageable pageable );

    public boolean deleteById(String _index, String _id);

    public Object getById(String _index, String _id, String _type);

}
