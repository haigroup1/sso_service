package com.savis.categories.service;

import com.savis.categories.dao.entity.DMThuTucHC;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DMThuTucHCService {

    List<DMThuTucHC> listDMThuTucHC( Integer appID ) ;

    Page<DMThuTucHC> listDMThuTucHCByApp( Integer appID, Pageable pageable ) ;

    Page<DMThuTucHC> getPage( Pageable pageable ) ;

    DMThuTucHC findByMaThuTuc(String maThuTuc, Integer appID);
}
