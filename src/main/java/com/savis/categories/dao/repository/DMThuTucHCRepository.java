package com.savis.categories.dao.repository;


import com.savis.categories.dao.entity.DMThuTucHC;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DMThuTucHCRepository extends JpaRepository<DMThuTucHC, Integer>, JpaSpecificationExecutor<DMThuTucHC> {
    List<DMThuTucHC> findAllByAppID( Integer appID ) ;

    Page<DMThuTucHC> findAllByAppID( Integer appID, Pageable pageable ) ;

    DMThuTucHC findByMaThuTucAndAndAppID(String maThuTuc, Integer appid);
}
