package com.savis.categories.dao.repository;



import com.savis.categories.dao.entity.adm.AdmUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdmUserRepository extends JpaRepository<AdmUser, Integer>, JpaSpecificationExecutor<AdmUser> {

	AdmUser findByUserId ( int UserId );

	AdmUser findByUserName ( String userName );

	List<AdmUser> findByApplicationId ( int applicationId );
}
