package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.AdmUser;
import com.savis.categories.dao.entity.adm.AdmUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
public interface AdmUserRoleRepository
		extends JpaRepository<AdmUserRole, Integer>, JpaSpecificationExecutor<AdmUserRole> {


	@Transactional
	@Modifying
	void deleteById(Integer id);

	AdmUserRole findById ( int id );

//	List<AdmUserRole> findByAdmUser ( AdmUser admUser );

	List<AdmUserRole> findByAdmUser ( Integer admUserID );

	@Transactional
	@Modifying
	AdmUserRole save(AdmUserRole admUserRole);
}
