package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ApplicationRepository extends JpaRepository<Application, Integer>, JpaSpecificationExecutor<Application> {

    Application findById(Integer id);

    @Transactional
    @Modifying
    @Query(value = "delete from Application where id = ?1", nativeQuery = true)
    void deleteById(Integer id);

}
