package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.HoSo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface HoSoRepository extends JpaRepository<HoSo, Integer>, JpaSpecificationExecutor<HoSo> {
    HoSo findById(int id);
}
