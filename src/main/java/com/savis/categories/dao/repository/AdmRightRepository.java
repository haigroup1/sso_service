package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.AdmRight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdmRightRepository extends JpaRepository<AdmRight, Integer>, JpaSpecificationExecutor<AdmRight> {
    AdmRight findByRightId(int rightId);

    List<AdmRight> findByHasChild(int hasChild);
}
