package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.AdmRoleApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdmRoleAppRepository extends JpaRepository<AdmRoleApp, Integer> {
    List<AdmRoleApp> findByAdmRole(Integer role_id);
}
