package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.AdmRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdmRoleRepository extends JpaRepository<AdmRole, Integer>, JpaSpecificationExecutor<AdmRole> {

	AdmRole findByRoleId ( int roleId );

	List<AdmRole> findByApplicationId ( int applicationId );
}
