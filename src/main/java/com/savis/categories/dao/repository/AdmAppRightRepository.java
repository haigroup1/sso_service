package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.adm.AdmAppRight;
import com.savis.categories.dao.entity.adm.AdmRight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AdmAppRightRepository extends JpaRepository<AdmAppRight, Integer> {
    List<AdmAppRight> findByAppID(Integer appid);

    @Transactional
    @Modifying
    @Query(value = "Delete from ADM_APP_RIGHT where APP_ID = ?1", nativeQuery = true)
    void deleteByAppID(Integer appid);
}
