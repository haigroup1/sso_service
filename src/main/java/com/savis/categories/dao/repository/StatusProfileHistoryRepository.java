package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.StatusProfileHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StatusProfileHistoryRepository extends JpaRepository<StatusProfileHistory, Integer> {
    public Page<StatusProfileHistory> findById( Integer id, Pageable pageable );
}
