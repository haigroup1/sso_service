package com.savis.categories.dao.repository;

import com.savis.categories.dao.entity.LichSuXuLy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LichSuXuLyRepository extends JpaRepository<LichSuXuLy, Integer> {
    Page<LichSuXuLy> findByHoSoID( Integer hsid, Pageable pageable );
}
