package com.savis.categories.dao.entity.dto;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data

public class LogDataDoiSoatDTO {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "profile_id")
    private Integer profile_id;

    @Column(name = "loai")
    private String loai;

    @Column(name = "ma_tham_chieu")
    private String ma_tham_chieu;

    @Column(name = "ma_ht_gui")
    private String ma_ht_gui;

    @Column(name = "ma_ht_nhan")
    private String ma_ht_nhan;

    @Column(name = "status_code")
    private String status_code;

    @Column(name = "type_request")
    private String type_request;

    @Column(name = "ngay_gui")
    private Date ngay_gui;

    @Column(name = "creation_date")
    private Date creation_date;

//    @Column(name = "data_input")
//    private Integer data_input;
//
//    @Column(name = "data_output")
//    private Integer data_output;

    @Column(name = "ma_hs_gui")
    private String ma_hs_gui;

    @Column(name = "ma_hs_nhan")
    private String ma_hs_nhan;
}
