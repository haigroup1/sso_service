package com.savis.categories.dao.entity.adm;
// Generated Nov 14, 2018 3:29:22 PM by Hibernate Tools 5.2.3.Final

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * AdmUserRole generated by hbm2java
 */
@Entity
@Table(name = "ADM_USER_ROLE")
@Builder
@Data
//@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class AdmUserRole implements java.io.Serializable {

	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer admRole;
	private Integer admUser;
	private String creator;
	private Date created;
	private String modifier;
	private Date modified;

	public AdmUserRole () {
	}

	public AdmUserRole ( Integer id, Integer admRole, Integer admUser) {
		this.id = id;
		this.admRole = admRole;
		this.admUser = admUser;
	}

	public AdmUserRole ( Integer id, Integer admRole, Integer admUser, String creator, Date created, String modifier,
                         Date modified) {
		this.id = id;
		this.admRole = admRole;
		this.admUser = admUser;
		this.creator = creator;
		this.created = created;
		this.modifier = modifier;
		this.modified = modified;
	}

	@Id
	@TableGenerator(name = "gen_id", table = "HIBERNATE_GEN_ID", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VALUE", allocationSize = 1)
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	@ManyToOne
//	@JoinColumn(name = "ROLE_ID", nullable = false)
	@Column(name = "ROLE_ID", nullable = false)
	public Integer getAdmRole() {
		return this.admRole;
	}

	public void setAdmRole(Integer admRole) {
		this.admRole = admRole;
	}

//	@ManyToOne
//	@JoinColumn(name = "USER_ID", nullable = false)
	@Column(name = "USER_ID", nullable = false)
	public Integer getAdmUser() {
		return this.admUser;
	}

	public void setAdmUser(Integer admUser) {
		this.admUser = admUser;
	}

	@Column(name = "CREATOR", length = 256)
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name = "CREATED")
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "MODIFIER", length = 256)
	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Column(name = "MODIFIED")
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Override
	public boolean equals(Object anObject) {
		if (!(anObject instanceof AdmUserRole)) {
			return false;
		}
		AdmUserRole otherMember = (AdmUserRole) anObject;
		return (otherMember.getAdmRole() == getAdmRole()
				&& otherMember.getAdmUser() == getAdmUser());
	}

}
