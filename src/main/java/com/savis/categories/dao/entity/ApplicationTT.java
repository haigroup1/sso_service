package com.savis.categories.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "applicationtt")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationTT {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "appid")
    private Integer appid;

    @Column(name = "tentrangthai")
    private String tentrangthai;

    @Column(name = "mota")
    private String mota;


    @Column(name = "matrangthai")
    private Integer matrangthai;

}
