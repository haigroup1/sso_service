package com.savis.categories.dao.entity.dto;

import com.savis.categories.dao.entity.adm.Application;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.variable.TableVariable;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variable;
import pl.jsolve.templ4docx.variable.Variables;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

@Component
@Getter
@Setter
public class WordUtil {

    public Docx getFileDoc ( HoSoSearchDTO object, List<HoSoResponse> profiles, InputStream path ) throws Exception {
        Docx docx = new Docx(path);
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        TableVariable tableVariable = new TableVariable();


        Variables variables = new Variables();
//        variables.addTextVariable(new TextVariable("${day}", "" + day));
//        variables.addTextVariable(new TextVariable("${month}", "" + (month+1)));
//        variables.addTextVariable(new TextVariable("${year}", "" + year));
//        variables.addTextVariable(new TextVariable("${startDate}",object.getFromDate() == null ? " " : String.valueOf(object.getFromDate())));
//        variables.addTextVariable(new TextVariable("${endDate}", object.getToDate() == null ? " " : String.valueOf(object.getToDate())));
//        variables.addTextVariable(new TextVariable("${appID}", object.getAppID() == null ? " " : String.valueOf(object.getAppID())));
//        variables.addTextVariable(new TextVariable("${tthcID}", object.getMaTTHC() == null ? " " : object.getMaTTHC()));
//        variables.addTextVariable(new TextVariable("${profileID}", object.getMaHoSo() == null ? " " :(object.getMaHoSo().equals("")? " ":object.getMaHoSo())));


        List<Variable> stt = new ArrayList<>();
        List<Variable> receiveDate = new ArrayList<>();
        List<Variable> app = new ArrayList<>();
        List<Variable> tthc = new ArrayList<>();
        List<Variable> profileTable  = new ArrayList<>();
        List<Variable> owner = new ArrayList<>();
        List<Variable> status = new ArrayList<>();
        if (profiles != null) {

            for (int i = 0; i < profiles.size(); i++) {
                stt.add(new TextVariable("${stt}", String.valueOf(i+1)));
                receiveDate.add(new TextVariable("${receiveDate}",String.valueOf(profiles.get(i).getNgayTiepNhan())));
                app.add(new TextVariable("${app}", String.valueOf(profiles.get(i).getAppID())));
                tthc.add(new TextVariable("${tthc}", String.valueOf(profiles.get(i).getMaTTHC())));
                profileTable.add(new TextVariable("${profileTable }", profiles.get(i).getMaHoSo()));
                owner.add(new TextVariable("${owner}", String.valueOf(profiles.get(i).getChuHoSo())));
                status.add(new TextVariable("${status}",
                        String.valueOf(profiles.get(i).getMaTrangThai() != null ? profiles.get(i).getTrangThaiHoSo() : " ")));
            }
            tableVariable.addVariable(stt);
            tableVariable.addVariable(receiveDate);
            tableVariable.addVariable(app);
            tableVariable.addVariable(tthc);
            tableVariable.addVariable(profileTable);
            tableVariable.addVariable(owner);
            tableVariable.addVariable(status);
            variables.addTableVariable(tableVariable);
        }
        docx.fillTemplate(variables);
        return docx;
    }


    public Docx getFileDocLog ( HoSoSearchDTO searchObject, List<Object> logs, InputStream resource, List<Application> listApp ) throws Exception {
        Docx docx = new Docx(resource);
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        TableVariable tableVariable = new TableVariable();


        Variables variables = new Variables();
//        variables.addTextVariable(new TextVariable("${day}", "" + day));
//        variables.addTextVariable(new TextVariable("${month}", "" + (month+1)));
//        variables.addTextVariable(new TextVariable("${year}", "" + year));
//        variables.addTextVariable(new TextVariable("${startDate}",object.getFromDate() == null ? " " : String.valueOf(object.getFromDate())));
//        variables.addTextVariable(new TextVariable("${endDate}", object.getToDate() == null ? " " : String.valueOf(object.getToDate())));
//        variables.addTextVariable(new TextVariable("${appID}", object.getAppID() == null ? " " : String.valueOf(object.getAppID())));
//        variables.addTextVariable(new TextVariable("${tthcID}", object.getMaTTHC() == null ? " " : object.getMaTTHC()));
//        variables.addTextVariable(new TextVariable("${profileID}", object.getMaHoSo() == null ? " " :(object.getMaHoSo().equals("")? " ":object.getMaHoSo())));


        List<Variable> stt = new ArrayList<>();
        List<Variable> receiveDate = new ArrayList<>();
        List<Variable> app = new ArrayList<>();
        List<Variable> tthc = new ArrayList<>();
        List<Variable> profileTable  = new ArrayList<>();
        if (logs != null) {

            for (int i = 0; i < logs.size(); i++) {
                LinkedHashMap<String, Object> mapHits = (LinkedHashMap<String, Object>) logs.get(i);
                LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) mapHits.get("_source");
                stt.add(new TextVariable("${stt}", String.valueOf(i+1)));
                receiveDate.add(new TextVariable("${receiveDate}", map.get("log_date").toString()));
                String appString = "Không tìm thấy";
                for(int j = 0; j<listApp.size(); j++) {
                    if(listApp.get(j).getId() == Integer.parseInt(map.get("appId").toString())) {
                        appString = listApp.get(j).getTenApp();
                        break;
                    }
                }
                app.add(new TextVariable("${app}", appString));
                try {
                    tthc.add(new TextVariable("${requestTable}", map.get("request_header_json").toString()));
                } catch (Exception ex) {
                    tthc.add(new TextVariable("${requestTable}", map.get("request_header").toString()));
                }
                try {
                    profileTable.add(new TextVariable("${responseTable}", map.get("response_body_json").toString()));
                } catch (Exception ex) {
                    profileTable.add(new TextVariable("${responseTable}", map.get("response_body").toString()));
                }
            }
            tableVariable.addVariable(stt);
            tableVariable.addVariable(receiveDate);
            tableVariable.addVariable(app);
            tableVariable.addVariable(tthc);
            tableVariable.addVariable(profileTable);
            variables.addTableVariable(tableVariable);
        }
        docx.fillTemplate(variables);
        return docx;
    }
}
