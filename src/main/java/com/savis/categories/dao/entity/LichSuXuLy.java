package com.savis.categories.dao.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "lich_su_xu_ly")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LichSuXuLy {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    Integer id;

    @Column(name = "hosoid")
    private Integer hoSoID;

    @Column(name = "malichsu")
    private Integer maLichSu;

    @Column(name = "ngayxuly")
    private Date ngayXuLy;

    @Column(name = "ngaybatdau")
    private Date ngayBatDau;

    @Column(name = "ngayketthuctheoquydinh")
    private Date ngayKetThucTheoQuyDinh;

    @Column(name = "canboxuly")
    private String canBoXuLy;

    @Column(name = "chucdanh")
    private String chucDanh;

    @Column(name = "donvixuly")
    private String donViXuLy;

    @Column(name = "noidungxuly")
    private String noiDungXyLy;

}
