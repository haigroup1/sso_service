package com.savis.categories.dao.entity.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfileReceiversResponseDTO {

    private Integer profileId;
    private Date ngayGui;
    private String loai;
}
