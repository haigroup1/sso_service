package com.savis.categories.dao.entity.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HoSoResponse {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "mahoso")
    private String maHoSo;

    @Column(name = "matthc")
    private String maTTHC;

    @Column(name = "kenhthuchien")
    private Integer kenhThucHien;

    @Column(name = "tendoanhnghiep")
    private String tenDoanhNghiep;

    @Column(name = "chuhoso")
    private String chuHoSo;

    @Column(name = "email")
    private String email;

    @Column(name = "fax")
    private String fax;

    @Column(name = "sodienthoai")
    private String soDienThoai;

    @Column(name = "ngaytiepnhan")
    private Date ngayTiepNhan;

    @Column(name = "ngayhentra")
    private Date ngayHenTra;

    @Column(name = "tentrangthai")
    private String trangThaiHoSo;

    @Column(name = "ngaytra")
    private Date ngayTra;

    @Column(name = "hinhthuc")
    private Integer hinhThuc;

    @Column(name = "ngayketthucxuly")
    private Date ngayKetThucXuLy;

    @Column(name = "tenapp")
    private String appID;

    @Column(name = "rawjson")
    private String rawjson;

    @Column(name = "documenttype")
    private String documenttype;

    @Column(name = "manoibo")
    private String manoibo;


    @Column(name = "madoanhnghiep")
    private String madoanhnghiep;

    @Column(name = "macoquancapdangky")
    private String macoquancapdangky;

    @Column(name = "loaihinhdoanhnghiep")
    private String loaihinhdoanhnghiep;

    @Column(name = "trangthaihoso")
    private Integer maTrangThai;

}
