package com.savis.categories.dao.entity.dto;

import java.util.List;

public class UserAD {

	private String displayName;
	private String givenName;
	private String sAMAccountType;
	private String primaryGroupID;
	private List<String> objectClass;
	private String badPasswordTime;
	private String objectCategory;
	private String cn;
	private String sn;
	private String userAccountControl;
	private String userPrincipalName;
	private String dSCorePropagationData;
	private String codePage;
	private String distinguishedName;
	private String whenChanged;
	private String whenCreated;
	private String pwdLastSet;
	private String logonCount;
	private String accountExpires;
	private String lastLogoff;
	private String lockoutTime;
	private String objectGUID;
	private String lastLogon;
	private String uSNChanged;
	private String uSNCreated;
	private String objectSid;

	/**
	 * @param displayName
	 * @param givenName
	 * @param sAMAccountType
	 * @param primaryGroupID
	 * @param objectClass
	 * @param badPasswordTime
	 * @param objectCategory
	 * @param cn
	 * @param sn
	 * @param userAccountControl
	 * @param userPrincipalName
	 * @param dSCorePropagationData
	 * @param codePage
	 * @param distinguishedName
	 * @param whenChanged
	 * @param whenCreated
	 * @param pwdLastSet
	 * @param logonCount
	 * @param accountExpires
	 * @param lastLogoff
	 * @param lockoutTime
	 * @param objectGUID
	 * @param lastLogon
	 * @param uSNChanged
	 * @param uSNCreated
	 * @param objectSid
	 */
	public UserAD ( String displayName, String givenName, String sAMAccountType, String primaryGroupID,
                    List<String> objectClass, String badPasswordTime, String objectCategory, String cn, String sn,
                    String userAccountControl, String userPrincipalName, String dSCorePropagationData, String codePage,
                    String distinguishedName, String whenChanged, String whenCreated, String pwdLastSet, String logonCount,
                    String accountExpires, String lastLogoff, String lockoutTime, String objectGUID, String lastLogon,
                    String uSNChanged, String uSNCreated, String objectSid) {
		super();
		this.displayName = displayName;
		this.givenName = givenName;
		this.sAMAccountType = sAMAccountType;
		this.primaryGroupID = primaryGroupID;
		this.objectClass = objectClass;
		this.badPasswordTime = badPasswordTime;
		this.objectCategory = objectCategory;
		this.cn = cn;
		this.sn = sn;
		this.userAccountControl = userAccountControl;
		this.userPrincipalName = userPrincipalName;
		this.dSCorePropagationData = dSCorePropagationData;
		this.codePage = codePage;
		this.distinguishedName = distinguishedName;
		this.whenChanged = whenChanged;
		this.whenCreated = whenCreated;
		this.pwdLastSet = pwdLastSet;
		this.logonCount = logonCount;
		this.accountExpires = accountExpires;
		this.lastLogoff = lastLogoff;
		this.lockoutTime = lockoutTime;
		this.objectGUID = objectGUID;
		this.lastLogon = lastLogon;
		this.uSNChanged = uSNChanged;
		this.uSNCreated = uSNCreated;
		this.objectSid = objectSid;
	}

	/**
	 * 
	 */
	public UserAD () {
		super();
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getsAMAccountType() {
		return sAMAccountType;
	}

	public void setsAMAccountType(String sAMAccountType) {
		this.sAMAccountType = sAMAccountType;
	}

	public String getPrimaryGroupID() {
		return primaryGroupID;
	}

	public void setPrimaryGroupID(String primaryGroupID) {
		this.primaryGroupID = primaryGroupID;
	}

	public List<String> getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(List<String> objectClass) {
		this.objectClass = objectClass;
	}

	public String getBadPasswordTime() {
		return badPasswordTime;
	}

	public void setBadPasswordTime(String badPasswordTime) {
		this.badPasswordTime = badPasswordTime;
	}

	public String getObjectCategory() {
		return objectCategory;
	}

	public void setObjectCategory(String objectCategory) {
		this.objectCategory = objectCategory;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getUserAccountControl() {
		return userAccountControl;
	}

	public void setUserAccountControl(String userAccountControl) {
		this.userAccountControl = userAccountControl;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public String getdSCorePropagationData() {
		return dSCorePropagationData;
	}

	public void setdSCorePropagationData(String dSCorePropagationData) {
		this.dSCorePropagationData = dSCorePropagationData;
	}

	public String getCodePage() {
		return codePage;
	}

	public void setCodePage(String codePage) {
		this.codePage = codePage;
	}

	public String getDistinguishedName() {
		return distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	public String getWhenChanged() {
		return whenChanged;
	}

	public void setWhenChanged(String whenChanged) {
		this.whenChanged = whenChanged;
	}

	public String getWhenCreated() {
		return whenCreated;
	}

	public void setWhenCreated(String whenCreated) {
		this.whenCreated = whenCreated;
	}

	public String getPwdLastSet() {
		return pwdLastSet;
	}

	public void setPwdLastSet(String pwdLastSet) {
		this.pwdLastSet = pwdLastSet;
	}

	public String getLogonCount() {
		return logonCount;
	}

	public void setLogonCount(String logonCount) {
		this.logonCount = logonCount;
	}

	public String getAccountExpires() {
		return accountExpires;
	}

	public void setAccountExpires(String accountExpires) {
		this.accountExpires = accountExpires;
	}

	public String getLastLogoff() {
		return lastLogoff;
	}

	public void setLastLogoff(String lastLogoff) {
		this.lastLogoff = lastLogoff;
	}

	public String getLockoutTime() {
		return lockoutTime;
	}

	public void setLockoutTime(String lockoutTime) {
		this.lockoutTime = lockoutTime;
	}

	public String getObjectGUID() {
		return objectGUID;
	}

	public void setObjectGUID(String objectGUID) {
		this.objectGUID = objectGUID;
	}

	public String getLastLogon() {
		return lastLogon;
	}

	public void setLastLogon(String lastLogon) {
		this.lastLogon = lastLogon;
	}

	public String getuSNChanged() {
		return uSNChanged;
	}

	public void setuSNChanged(String uSNChanged) {
		this.uSNChanged = uSNChanged;
	}

	public String getuSNCreated() {
		return uSNCreated;
	}

	public void setuSNCreated(String uSNCreated) {
		this.uSNCreated = uSNCreated;
	}

	public String getObjectSid() {
		return objectSid;
	}

	public void setObjectSid(String objectSid) {
		this.objectSid = objectSid;
	}

}
