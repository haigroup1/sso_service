package com.savis.categories.dao.entity.dto;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StatusMappingDTO {
    @Id
    @Column(name = "appid")
    private Integer appid;

    @Column(name = "matrangthai")
    private Integer matrangthai;

    @Column(name = "tentrangthai")
    private String tentrangthai;

    @Column(name = "idtrangthaidvc")
    private Integer idTrangThaidDVC;

    @Column(name = "tentrangthaidvc")
    private String tenTrangThaiDVC;

    @Column(name = "ghichu")
    private String ghiChu;

    @Column(name = "status")
    private Integer status;

}
