package com.savis.categories.dao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.savis.categories.dao.entity.adm.Application;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "hoso")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HoSo implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "mahoso")
    private String maHoSo;

    @Column(name = "matthc")
    private String maTTHC;

    @Column(name = "kenhthuchien")
    private Integer kenhThucHien;

    @Column(name = "tendoanhnghiep")
    private String tenDoanhNghiep;

    @Column(name = "chuhoso")
    private String chuHoSo;

    @Column(name = "email")
    private String email;

    @Column(name = "fax")
    private String fax;

    @Column(name = "sodienthoai")
    private String soDienThoai;

    @Column(name = "ngaytiepnhan")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
    private Date ngayTiepNhan;

    @Column(name = "ngayhentra")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
    private Date ngayHenTra;

    @Column(name = "trangthaihoso")
    private Integer trangThaiHoSo;

    @Column(name = "ngaytra")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
    private Date ngayTra;

    @Column(name = "hinhthuc")
    private Integer hinhThuc;

    @Column(name = "ngayketthucxuly")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
    private Date ngayKetThucXuLy;


//    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "appid")
    private Application appID;

//    @JsonIgnore
    @Column(name = "rawjson")
    private String rawjson;

    @Column(name = "documenttype")
    private String documenttype;

    @Column(name = "manoibo")
    private String manoibo;


    @Column(name = "madoanhnghiep")
    private String madoanhnghiep;

    @Column(name = "macoquancapdangky")
    private String macoquancapdangky;

    @Column(name = "loaihinhdoanhnghiep")
    private String loaihinhdoanhnghiep;

    @Column(name = "trangthaitrenbo")
    private String trangthaitrenbo;
}
