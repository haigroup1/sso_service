package com.savis.categories.dao.entity.adm;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ADM_ROLE_APP")
@Builder
@Data
public class AdmRoleApp {
    private Integer id;
    private Integer admRole;
    private Integer appid;
    private String creator;
    private Date created;
    private String modifier;
    private Date modified;

    public AdmRoleApp () {
    }

    public AdmRoleApp ( Integer id, Integer admRole, Integer appid ) {
        this.id = id;
        this.admRole = admRole;
        this.appid = appid;
    }

    public AdmRoleApp ( Integer id, Integer admRole, Integer appid, String creator, Date created, String modifier, Date modified ) {
        this.id = id;
        this.admRole = admRole;
        this.appid = appid;
        this.creator = creator;
        this.created = created;
        this.modifier = modifier;
        this.modified = modified;
    }

    @Id
    @TableGenerator(name = "gen_id", table = "HIBERNATE_GEN_ID", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VALUE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //	@ManyToOne
//	@JoinColumn(name = "ROLE_ID", nullable = false)
    @Column(name = "ROLE_ID", nullable = false)
    public Integer getAdmRole() {
        return this.admRole;
    }

    public void setAdmRole(Integer admRole) {
        this.admRole = admRole;
    }

    //	@ManyToOne
//	@JoinColumn(name = "USER_ID", nullable = false)
    @Column(name = "APP_ID", nullable = false)
    public Integer getAppID() {
        return this.appid;
    }

    public void setAppID(Integer admUser) {
        this.appid = admUser;
    }

    @Column(name = "CREATOR", length = 256)
    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Column(name = "CREATED")
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Column(name = "MODIFIER", length = 256)
    public String getModifier() {
        return this.modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    @Column(name = "MODIFIED")
    public Date getModified() {
        return this.modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
