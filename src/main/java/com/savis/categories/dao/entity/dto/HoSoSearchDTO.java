package com.savis.categories.dao.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HoSoSearchDTO {

    private Integer appID;
    private String maTTHC;
    private Date fromDate;
    private Date toDate;
    private String maHoSo;
    private String chooseDate;
    private String chuHoSo;

    private Date formDate;//ngày ghi log
    private String apiPath;// path api
    //trong header
    private String idReceivedDec;
    private String idMoneyReceipt;
    private String infoType;
    private Date datePromissoryDecFrom;
    private Date datePromissoryDecTo;
    //trong body
    private String decStatusId;
    private String status;
    private String description;
    private String appIdElastic;
    private Date fromLogDate;
    private Date toLogDate;
}

