package com.savis.categories.dao.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CountStatusProfileDTO {
    @Id
    @Column(name = "ngay")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date ngay;
    @Column(name = "dangxuly")
    private Integer dangxuly;
    @Column(name = "dacokq")
    private Integer dacokq;
    @Column(name = "dahuy")
    private Integer dahuy;
}
