package com.savis.categories.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
//@Table(name = "lich_su_status")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StatusProfileHistory implements Serializable {
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    Integer id;

    @Column(name = "appid")
    private Integer appid;

    @Column(name = "mahoso")
    private String maHoSo;

    @Column(name = "ngay_cap_phieu")
    private String ngayCapPhieu;

    @Column(name = "ngay_tra")
    private String ngayTra;


    @Column(name = "matrangthai")
    private String maTrangThai;

    @Column(name = "tentrangthai")
    private String tenTrangThai;

    @Id
    @Column(name = "ma_trang_thai")
    private String maTrangThaiGui;

    @Column(name = "tentrangthaigui")
    private String tenTrangThaiGui;

}
