package com.savis.categories.dao.entity.adm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.AdmUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "application")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Application {
    @Id
    @TableGenerator(name = "gen_id", table = "HIBERNATE_GEN_ID", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VALUE", allocationSize = 1)
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "tenapp")
    private String tenApp;

    @Column(name = "mota")
    private String moTa;

    @Column(name = "donvixuly")
    private String donvixuly;

    @OneToMany(targetEntity= AdmRole.class, mappedBy = "applicationList", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AdmRole> admRoles;

    @ManyToMany(cascade = { CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "ADM_APP_RIGHT",
            joinColumns = @JoinColumn(name = "APP_ID"),
            inverseJoinColumns = @JoinColumn(name = "RIGHT_ID")
    )
    private List<AdmRight> admRights;

}
