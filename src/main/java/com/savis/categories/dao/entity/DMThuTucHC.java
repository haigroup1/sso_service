package com.savis.categories.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NaturalId;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dmthutuchc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DMThuTucHC.findAll", query = "SELECT d FROM DMThuTucHC d")
    , @NamedQuery(name = "DMThuTucHC.findById", query = "SELECT d FROM DMThuTucHC d WHERE d.id = :id")
    , @NamedQuery(name = "DMThuTucHC.findByAppID", query = "SELECT d FROM DMThuTucHC d WHERE d.appID = :appID")
    , @NamedQuery(name = "DMThuTucHC.findByMaThuTuc", query = "SELECT d FROM DMThuTucHC d WHERE d.maThuTuc = :maThuTuc")
    , @NamedQuery(name = "DMThuTucHC.findByTenThuTuc", query = "SELECT d FROM DMThuTucHC d WHERE d.tenThuTuc = :tenThuTuc")})
public class DMThuTucHC implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "appid")
    private Integer appID;

    @Column(name = "mathutuc")
    private String maThuTuc;

    @Column(name = "tenthutuc")
    private String tenThuTuc;

    public DMThuTucHC() {
    }

    public DMThuTucHC(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppID() {
        return appID;
    }

    public void setAppID(Integer appID) {
        this.appID = appID;
    }

    public String getMaThuTuc() {
        return maThuTuc;
    }

    public void setMaThuTuc(String maThuTuc) {
        this.maThuTuc = maThuTuc;
    }

    public String getTenThuTuc() {
        return tenThuTuc;
    }

    public void setTenThuTuc(String tenThuTuc) {
        this.tenThuTuc = tenThuTuc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DMThuTucHC)) {
            return false;
        }
        DMThuTucHC other = (DMThuTucHC) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "render.table.DMThuTucHC[ id=" + id + " ]";
    }
    
}
