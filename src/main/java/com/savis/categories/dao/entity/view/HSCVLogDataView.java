package com.savis.categories.dao.entity.view;

import com.savis.categories.endpoint.dto.common.JsonHelper;
import lombok.*;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class HSCVLogDataView implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "profile_id")
    private Integer profileId;

    @Column(name = "loai")
    private String loai;

    @Column(name = "ma_tham_chieu")
    private String maThamChieu;

    @Column(name = "ma_ht_gui")
    private String maHTGui;

    @Column(name = "ma_ht_nhan")
    private String maHTNhan;

    @Column(name = "status_code")
    private Integer status_code;

    @Column(name = "type_request")
    private String type_request;

    @Column(name = "ngay_gui")
    private Date ngayGui;

    @Column(name = "creation_date")
    private Date creation_date;

    @Column(name = "data_input")
    private String data_input;

    @Column(name = "data_output")
    private String data_output;

    @Column(name = "ma_hs_gui")
    private String maHSGui	;

    @Column(name = "ma_hs_nhan")
    private String maHSNhan	;

    @Transient
    private Object data_input_o;
    @Transient
    private Object data_output_o;

    public Object getData_input_o() {
        if (StringUtils.isNotBlank(data_input)) {
            data_input_o = JsonHelper.jsonToObject(data_input, Object.class);
        }
        return data_input_o;
    }

    public Object getData_output_o() {
        if (StringUtils.isNotBlank(data_output)) {
            data_output_o = JsonHelper.jsonToObject(data_output, Object.class);
        }
        return data_output_o;
    }
}
