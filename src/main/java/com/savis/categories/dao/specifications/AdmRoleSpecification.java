package com.savis.categories.dao.specifications;

import com.savis.categories.dao.entity.adm.AdmRole;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AdmRoleSpecification {

	public static Specification<AdmRole> advanceSearch(AdmRole admRole) {
		return new Specification<AdmRole>() {

			@Override
			public Predicate toPredicate(Root<AdmRole> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;
				// search in User Name column
				if (admRole.getRoleName() != null) {
					obj = cb.like(cb.lower(root.get("roleName")), "%" + admRole.getRoleName() + "%");
					predicateList.add(obj);

				}

				if (admRole.getRoleCode() != null) {
					obj = cb.like(cb.lower(root.get("roleCode")), "%" + admRole.getRoleCode() + "%");
					predicateList.add(obj);

				}

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}

	public static Specification<AdmRole> filterSearch( String filter) {
		return new Specification<AdmRole>() {


			@Override
			public Predicate toPredicate(Root<AdmRole> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;

				if (filter != null) {
					// search in Role name column
					obj = cb.like(cb.lower(root.get("roleName")), "%" + filter.toLowerCase() + "%");
					predicateList.add(obj);

					// search in Role method column
					obj = cb.like(cb.lower(root.get("description")), "%" + filter.toLowerCase() + "%");
					predicateList.add(obj);

				}

				return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
