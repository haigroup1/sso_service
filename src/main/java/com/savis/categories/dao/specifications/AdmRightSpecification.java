package com.savis.categories.dao.specifications;

import com.savis.categories.dao.entity.adm.AdmRight;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AdmRightSpecification {

	public static Specification<AdmRight> advanceSearch( AdmRight admRight) {
		return new Specification<AdmRight>() {

			@Override
			public Predicate toPredicate(Root<AdmRight> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;
				// search in Right name column
				if (admRight.getRightName() != null) {
					obj = cb.like(cb.lower(root.get("rightName")), "%" + admRight.getRightName() + "%");
					predicateList.add(obj);

				}
				// search in Right method column
				if (admRight.getRightCode() != null) {
					obj = cb.like(cb.lower(root.get("rightCode")), "%" + admRight.getRightCode() + "%");
					predicateList.add(obj);

				}

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}

	public static Specification<AdmRight> filterSearch(String filter) {
		return new Specification<AdmRight>() {

			@Override
			public Predicate toPredicate(Root<AdmRight> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;

				if (filter != null) {
					// search in Right name column
					obj = cb.like(cb.lower(root.get("rightName")), "%" + filter.toLowerCase() + "%");
					predicateList.add(obj);

					// search in Right method column
					obj = cb.like(cb.lower(root.get("description")), "%" + filter.toLowerCase() + "%");
					predicateList.add(obj);

				}

				return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
