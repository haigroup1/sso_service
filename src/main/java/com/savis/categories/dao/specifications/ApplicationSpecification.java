package com.savis.categories.dao.specifications;

import com.savis.categories.dao.entity.adm.Application;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ApplicationSpecification {

    public static Specification<Application> advanceSearch(Application application) {
        return new Specification<Application>() {
            @Override
            public Predicate toPredicate ( Root<Application> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder ) {
                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;
                // search in User Name column
                if (application.getTenApp() != null) {
                    if(!application.getTenApp().isEmpty()) {
                        obj = criteriaBuilder.like(criteriaBuilder.lower(root.get("tenApp")), "%" + application.getTenApp() + "%");
                        predicateList.add(obj);
                    }

                }

                if (application.getMoTa() != null ) {
                    if(!application.getMoTa().isEmpty()) {
                        obj = criteriaBuilder.like(criteriaBuilder.lower(root.get("moTa")), "%" + application.getMoTa() + "%");
                        predicateList.add(obj);
                    }
                }

                return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

}
