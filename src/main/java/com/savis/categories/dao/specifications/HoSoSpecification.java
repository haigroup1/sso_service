package com.savis.categories.dao.specifications;

import com.savis.categories.dao.entity.HoSo;
import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class HoSoSpecification {


    public static Specification<HoSo> filterSearch(String filter) {
        return new Specification<HoSo>() {

            @Override
            public Predicate toPredicate(Root<HoSo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;

                if (filter != null) {
                    obj = cb.equal(root.get("appID"), filter);
                    predicateList.add(obj);

                    obj = cb.equal(root.get("maTTHC"), filter);
                    predicateList.add(obj);

                    obj = cb.greaterThanOrEqualTo(root.get("ngayTiepNhan"), filter);
                    predicateList.add(obj);

                    obj = cb.greaterThanOrEqualTo(root.get("ngayTiepNhan"), filter);
                    predicateList.add(obj);

                    obj = cb.like(cb.lower(root.get("maHoSo")), "%" + filter.toLowerCase() + "%");
                    predicateList.add(obj);
                }

                query.orderBy(cb.desc(root.get("ngayTiepNhan")));

                return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

    public static Specification<HoSo> advanceSearch(HoSoSearchDTO hoSoSearchDTO) {
        return new Specification<HoSo>() {
            @Value("${delete.fax}")
            private String deleteValue;

            @Override
            public Predicate toPredicate(Root<HoSo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;
                // search in api name column

                if(hoSoSearchDTO.getAppID() != null) {
                    obj = cb.equal(root.get("appID"), hoSoSearchDTO.getAppID());
                    predicateList.add(obj);
                }

                if (hoSoSearchDTO.getMaTTHC() != null ) {
                    if(!hoSoSearchDTO.getMaTTHC().isEmpty()) {
                        obj = cb.equal(root.get("maTTHC"), hoSoSearchDTO.getMaTTHC());
                        predicateList.add(obj);
                    }
                }
                if (hoSoSearchDTO.getFromDate() != null) {
                    obj = cb.greaterThanOrEqualTo(root.get("ngayTiepNhan"), hoSoSearchDTO.getFromDate());
                    predicateList.add(obj);
                }
                if (hoSoSearchDTO.getToDate() != null) {
                    obj = cb.lessThanOrEqualTo(root.get("ngayTiepNhan"), hoSoSearchDTO.getToDate());
                    predicateList.add(obj);
                }
                if (hoSoSearchDTO.getMaHoSo() != null ) {
                    if(!hoSoSearchDTO.getMaHoSo().isEmpty()) {
                        obj = cb.like(cb.lower(root.get("maHoSo")), "%"+hoSoSearchDTO.getMaHoSo()+"%");
                        predicateList.add(obj);
                    }
                }
//                obj = cb.notEqual(root.get("fax"), deleteValue);
//                predicateList.add(obj);
//                obj = cb.or(cb.isNull(root.get("fax")));
//                predicateList.add(obj);
//                obj = cb.notEqual(root.get("appID"), 0);
//                predicateList.add(obj);
                query.orderBy(cb.desc(root.get("ngayTiepNhan")));

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }
}
