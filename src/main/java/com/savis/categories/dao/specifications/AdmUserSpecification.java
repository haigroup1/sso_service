package com.savis.categories.dao.specifications;

import com.savis.categories.dao.entity.adm.AdmUser;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AdmUserSpecification {

	public static Specification<AdmUser> advanceSearch( AdmUser admUser) {
		return new Specification<AdmUser>() {

			@Override
			public Predicate toPredicate(Root<AdmUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;
				// search in User Name column
				if (admUser.getUserName() != null) {
					obj = cb.like(cb.lower(root.get("userName")), "%" + admUser.getUserName() + "%");
					predicateList.add(obj);

				}
				
				if (admUser.getMobileAlias() != null) {
					obj = cb.like(cb.lower(root.get("mobileAlias")), "%" + admUser.getMobileAlias() + "%");
					predicateList.add(obj);

				}

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
	
	public static Specification<AdmUser> filterSearch(String filter) {
		return new Specification<AdmUser>() {

			@Override
			public Predicate toPredicate(Root<AdmUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;

				if (filter != null) {
					// search in User name column
					obj = cb.like(cb.lower(root.get("userName")), "%" + filter.toLowerCase() + "%");
					predicateList.add(obj);

				}

				return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
