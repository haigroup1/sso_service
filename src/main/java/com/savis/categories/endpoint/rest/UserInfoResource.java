package com.savis.categories.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.savis.categories.common.exception.Result;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.auth.dto.user.AccessTokenInfo;
import com.savis.categories.auth.dto.user.UserInfo;
import com.savis.categories.service.IsUserService;

@Controller
@RequestMapping("/savis/categories/api/v1/sso")
public class UserInfoResource {

	@Autowired
	private IsUserService isUserService;

	@RequestMapping(value = "/user-info", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<UserInfo>> getAuthenticatedInfo(@RequestBody String authorizationCode) {
		ResponseData<UserInfo> response = new ResponseData<UserInfo>();
		UserInfo userInfo = isUserService.getUserInfo(authorizationCode);
		if (userInfo != null) {
			response = new ResponseData<UserInfo>(userInfo, Result.SUCCESS);
			return new ResponseEntity<ResponseData<UserInfo>>(response, HttpStatus.OK);
		}
		response = new ResponseData<UserInfo>(userInfo, Result.NO_CONTENT);
		return new ResponseEntity<ResponseData<UserInfo>>(response, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/access-token", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<AccessTokenInfo>> getAccessToken(@RequestBody String refreshToken) {
		ResponseData<AccessTokenInfo> response = new ResponseData<AccessTokenInfo>();
		AccessTokenInfo accessTokenInfo = isUserService.getAccessToken(refreshToken);
		if (accessTokenInfo != null) {
			response = new ResponseData<AccessTokenInfo>(accessTokenInfo, Result.SUCCESS);
			return new ResponseEntity<ResponseData<AccessTokenInfo>>(response, HttpStatus.OK);
		}
		response = new ResponseData<AccessTokenInfo>(accessTokenInfo, Result.NO_CONTENT);
		return new ResponseEntity<ResponseData<AccessTokenInfo>>(response, HttpStatus.NO_CONTENT);
	}
}
