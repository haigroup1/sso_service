package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.adm.application.ApplicationDTO;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.Application;
import com.savis.categories.endpoint.dto.common.JsonHelper;
import com.savis.categories.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/app")
public class ApplicationResource {

    @Autowired
    private ApplicationService applicationService;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<List<Application>>> getList() {
        ResponseData<List<Application>> response = new ResponseData<>();
        List<Application> listApi = null;
        listApi = applicationService.getListApplication();
        if (listApi != null) {
            response = new ResponseData<List<Application>>(listApi, Result.SUCCESS);
            return new ResponseEntity<ResponseData<List<Application>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<Application>>(listApi, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<List<Application>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/page", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<Page<Application>>> getPage( Pageable pageable,
                                                                @RequestParam(value = "search", required = false) String search) {
        ResponseData<Page<Application>> response = new ResponseData<>();
        Page<Application> pageApp = null;
        Application searchObject = JsonHelper.jsonToObject(search, Application.class);

        // advance search
        if (searchObject != null) {
            pageApp = applicationService.getPageAppliaction(searchObject, pageable);
        }

        if (pageApp != null) {
            response = new ResponseData<Page<Application>>(pageApp, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<Application>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<Application>>(pageApp, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<Application>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<Application>> findOne(@PathVariable("id") int id) {
        ResponseData<Application> response = new ResponseData<Application>();
        Application application = applicationService.getOne(id);
        if (application != null) {
            response = new ResponseData<Application>(application, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Application>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Application>(application, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Application>>(response, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Integer>> create(@RequestBody ApplicationDTO applicationDTO) {
        Result result = applicationService.create(applicationDTO);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<ResponseData<Integer>> update(@RequestBody ApplicationDTO applicationDTO) {
        Result result = applicationService.update(applicationDTO);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> delete(@PathVariable("id") Integer id) {
        Result result = applicationService.delete(id);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> deleteAllBatch(@RequestParam("entityIds") int[] entityIds) {
        Result result = applicationService.delete(entityIds);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }


}
