package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.adm.user.AdmUserDTO;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.AdmUser;
import com.savis.categories.dao.repository.AdmRoleRepository;
import com.savis.categories.endpoint.dto.common.JsonHelper;
import com.savis.categories.service.AdmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author
 * @created
 * 
 * @modified 20/11/2018
 * @modifier LongTT
 */
@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/autho/api/v1/users")
public class AdmUserResource {

	@Autowired
	private AdmUserService admUserService;

	@Autowired
	private AdmRoleRepository admRoleRepository;

	@GetMapping(value = "", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Page<AdmUser>>> getPage( Pageable pageable,
																@RequestParam(value = "search", required = false) String search,
																@RequestParam(value = "filter", required = false) String filter) {
		ResponseData<Page<AdmUser>> response = new ResponseData<>();
		Page<AdmUser> pageUser = null;
		AdmUser searchObject = JsonHelper.jsonToObject(search, AdmUser.class);

		// advance search
		if (searchObject != null) {
			pageUser = admUserService.getPage(pageable, searchObject);
		}
		// filter search
		else if (filter != null) {
			pageUser = admUserService.getPage(pageable, filter);
		}
		// get page default
		else {
			pageUser = admUserService.getPage(pageable);
		}

		if (pageUser != null) {
			response = new ResponseData<Page<AdmUser>>(pageUser, Result.SUCCESS);
			return new ResponseEntity<ResponseData<Page<AdmUser>>>(response, HttpStatus.OK);
		}
		response = new ResponseData<Page<AdmUser>>(pageUser, Result.NO_CONTENT);
		return new ResponseEntity<ResponseData<Page<AdmUser>>>(response, HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "/list", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<List<AdmUser>>> getList() {
		ResponseData<List<AdmUser>> response = new ResponseData<>();
		List<AdmUser> listUser = null;
		listUser = admUserService.getList();
		if (listUser != null) {
			response = new ResponseData<List<AdmUser>>(listUser, Result.SUCCESS);
			return new ResponseEntity<ResponseData<List<AdmUser>>>(response, HttpStatus.OK);
		}
		response = new ResponseData<List<AdmUser>>(listUser, Result.NO_CONTENT);
		return new ResponseEntity<ResponseData<List<AdmUser>>>(response, HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<AdmUser>> findOne(@PathVariable("id") int id) {
		ResponseData<AdmUser> response = new ResponseData<AdmUser>();
		AdmUser admUser = admUserService.getOne(id);
		if (admUser != null) {
			response = new ResponseData<AdmUser>(admUser, Result.SUCCESS);
			return new ResponseEntity<ResponseData<AdmUser>>(response, HttpStatus.OK);
		}
		response = new ResponseData<AdmUser>(admUser, Result.NO_CONTENT);
		return new ResponseEntity<ResponseData<AdmUser>>(response, HttpStatus.NO_CONTENT);
	}

//	@GetMapping(value = "/{id}/user-role", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
//	public ResponseEntity<ResponseData<List<AdmRole>>> getRoleByUser( @PathVariable("id") int id) {
//		ResponseData<List<AdmRole>> response = new ResponseData<List<AdmRole>>();
//		List<AdmRole> admUserRoles = admRoleRepository.findByApplicationId(id);
//		if (admUserRoles != null) {
//			response = new ResponseData<List<AdmRole>>(admUserRoles, Result.SUCCESS);
//			return new ResponseEntity<ResponseData<List<AdmRole>>>(response, HttpStatus.OK);
//		}
//		response = new ResponseData<List<AdmRole>>(admUserRoles, Result.NO_CONTENT);
//		return new ResponseEntity<ResponseData<List<AdmRole>>>(response, HttpStatus.NO_CONTENT);
//	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<ResponseData<Integer>> create(@RequestBody AdmUserDTO admUserDTO) {
		admUserDTO.setUserId(null);
		Result result = admUserService.create(admUserDTO);
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<ResponseData<Integer>> update(@RequestBody AdmUserDTO admUserDTO) {
		Result result = admUserService.update(admUserDTO);
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseData<Integer>> delete(@PathVariable("id") Integer id) {
		Result result = admUserService.delete(id);
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseData<Integer>> deleteAllBatch(@RequestParam("entityIds") int[] entityIds) {
		Result result = admUserService.delete(entityIds);
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
	}

}
