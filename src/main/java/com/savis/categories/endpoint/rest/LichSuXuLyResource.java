package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.HoSo;
import com.savis.categories.dao.entity.LichSuXuLy;
import com.savis.categories.dao.repository.LichSuXuLyRepository;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/handle-history")
public class LichSuXuLyResource {

    @Autowired
    LichSuXuLyRepository lichSuXuLyRepository;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<Page<LichSuXuLy>>> findOne ( @RequestParam("hsid") Integer hsid, Pageable pageable ) {
        ResponseData<Page<LichSuXuLy>> response = new ResponseData<Page<LichSuXuLy>>();
        Page<LichSuXuLy> lichSuXuLys = lichSuXuLyRepository.findByHoSoID(hsid, pageable);
        if (lichSuXuLys != null) {
            response = new ResponseData<Page<LichSuXuLy>>(lichSuXuLys, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<LichSuXuLy>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<LichSuXuLy>>(lichSuXuLys, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<LichSuXuLy>>>(response, HttpStatus.NO_CONTENT);
    }
}
