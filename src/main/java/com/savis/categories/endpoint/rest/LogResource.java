package com.savis.categories.endpoint.rest;


// import com.savis.categories.dao.entity.dto.HSCongViecLogSearchObject;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.Application;
import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import com.savis.categories.dao.entity.dto.WordUtil;
import com.savis.categories.endpoint.dto.common.JsonHelper;
import com.savis.categories.service.ApplicationService;
import com.savis.categories.service.LogProfileService;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.jsolve.templ4docx.core.Docx;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/log")
public class LogResource {

        @Autowired
        LogProfileService logProfileService;

        @Autowired
        ApplicationService applicationService;

        @RequestMapping(value = "", method = RequestMethod.GET)
        public @ResponseBody
        ResponseEntity<ResponseData<Object>>  getByIdQuery( @RequestParam(value = "index",required = false) String index,
                                              @RequestParam(value = "id",required = false) String id, @RequestParam(value = "type",required = false) String _type) {
                ResponseData<Object> response = new ResponseData<Object>();
                Object object = logProfileService.getById(index, id, _type);
                if (object != null) {
                        response = new ResponseData<Object>(object, Result.SUCCESS);
                        return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
                }
                response = new ResponseData<Object>(object, Result.NO_CONTENT);
                return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.NO_CONTENT);
        }

        @RequestMapping(value = "/list", method = RequestMethod.GET)
        public @ResponseBody
        ResponseData<Page<Object>> getByQuery( @RequestParam(required = false) String search, Pageable pageable) {
                ResponseData<Page<Object>> response = new ResponseData<>();
                HoSoSearchDTO searchForm = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
                Page<Object> pay = null ;
                try {
                        pay = logProfileService.getLog(searchForm, pageable);
                        response.setData(pay);
                        response.setCode(200);
                        response.setMessage("OK");
                } catch (Exception e) {
                        e.printStackTrace();
                        response.setData(null);
                        response.setCode(500);
                        response.setMessage("ERROR");
                }
                return response;
        }

        @RequestMapping(value = "/list", method = RequestMethod.DELETE)
        public @ResponseBody
        ResponseEntity<Boolean>  deleteQuery( @RequestParam(value = "index",required = false) String index,
                                              @RequestParam(value = "id",required = false) String id) {
                Boolean hoSoDelete = logProfileService.deleteById(index, id);
                ResponseEntity<Boolean> response = null;
                try {
                        response = new ResponseEntity<Boolean>(hoSoDelete, HttpStatus.OK);

                } catch (Exception e) {
                        e.printStackTrace();
                        response = new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
                }
                return response;
        }


        @RequestMapping(value = "/list-log", method = RequestMethod.DELETE)
        public ResponseEntity<Boolean> deleteAllPGML( @RequestParam(required = false) String[] indexs, String[] ids) {
                ResponseEntity<Boolean> response = null;
                try {
                        for(int i = 0; i < ids.length; i++) {
                                Boolean hoSoDelete = logProfileService.deleteById(indexs[i], ids[i]);
                        }
                        response = new ResponseEntity<Boolean>(true, HttpStatus.OK);

                } catch (Exception e) {
                        e.printStackTrace();
                        response = new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
                }
                return response;
        }

        @GetMapping("/list/export-file")
        public void exportWordLog( HttpServletResponse response, @RequestParam(required = false) String search, Pageable pageable) {
                try {
                        Integer opptionDownload = 1;
                        HoSoSearchDTO searchObject = com.savis.categories.auth.dto.common.JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
                        Page<Object> hoSos = logProfileService.getLog(searchObject, pageable);
                        SimpleDateFormat dateFormatHeader = new SimpleDateFormat("dd?MM/yyyy");
                        DateFormat dateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");
                        DateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
                        response.setHeader("Content-Disposition", "attachment; filename= DanhSachLog" + dateFormatHeader.format(new Date()) + ".docx");
                        ServletOutputStream out = response.getOutputStream();
                        InputStream resource = new ClassPathResource("/file/logTemplateAuction.docx").getInputStream();
                        WordUtil wordUtil = new WordUtil();
                        List<Application> listApp = applicationService.getListApplication();
                        Docx docx = wordUtil.getFileDocLog(searchObject, hoSos.getContent(), resource, listApp);
                        XWPFDocument documentOutPut = docx.getXWPFDocument();
                        if (opptionDownload == 1) {
                                PdfOptions options = PdfOptions.create();
                                OutputStream outputStream = new FileOutputStream("LogDocument.pdf");
                                PdfConverter.getInstance().convert(documentOutPut, outputStream, options);
                                response.setHeader("Content-Disposition", "attachment; filename= DanhSachLog" + dateFormatHeader.format(new Date()) + ".pdf");
                                InputStreamResource fileResource = new InputStreamResource(new FileInputStream("LogDocument.pdf"));
                                InputStream is = fileResource.getInputStream();
                                byte[] bytes = new byte[1024];
                                int bytesRead;
                                while ((bytesRead = is.read(bytes)) != -1) {
                                        // Ghi dữ liệu ảnh vào Response.
                                        response.getOutputStream().write(bytes, 0, bytesRead);
                                }
                                is.close();
                                out.flush();
                                out.close();
                        }
                        documentOutPut.write(out);
                        out.flush();
                        out.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }


}
