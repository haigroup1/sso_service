package com.savis.categories.endpoint.rest;


import com.savis.categories.auth.dto.adm.right.AdmRightDTO;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRight;
import com.savis.categories.endpoint.dto.common.JsonHelper;
import com.savis.categories.service.AdmRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/savis/autho/api/v1/rights")
public class AdmRightResource {
    @Autowired
    private AdmRightService admRightService;

    @GetMapping(value = "", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<Page<AdmRight>>> getPage( Pageable pageable,
                                                                 @RequestParam(value = "search", required = false) String search,
                                                                 @RequestParam(value = "filter", required = false) String filter) {
        ResponseData<Page<AdmRight>> response = new ResponseData<>();
        Page<AdmRight> pageRight = null;
        AdmRight searchObject = JsonHelper.jsonToObject(search, AdmRight.class);

        // advance search
        if (searchObject != null) {
            pageRight = admRightService.getPage(pageable, searchObject);
        }
        // filter search
        else if (filter != null) {
            pageRight = admRightService.getPage(pageable, filter);
        }
        // get page default
        else {
            pageRight = admRightService.getPage(pageable);
        }

        if (pageRight != null) {
            response = new ResponseData<Page<AdmRight>>(pageRight, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<AdmRight>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<AdmRight>>(pageRight, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<AdmRight>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/list", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<List<AdmRight>>> getList() {
        ResponseData<List<AdmRight>> response = new ResponseData<>();
        List<AdmRight> listRight = null;
        listRight = admRightService.getList();
        if (listRight != null) {
            Comparator<AdmRight> comparator = ( AdmRight a1, AdmRight a2) -> a1.getRightOrder() < a2.getRightOrder() ? 1:0;
            listRight.sort(comparator);
//            System.out.println(listRight.toArray().toString());
            response = new ResponseData<List<AdmRight>>(listRight, Result.SUCCESS);

            return new ResponseEntity<ResponseData<List<AdmRight>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<AdmRight>>(listRight, Result.NO_CONTENT);

        return new ResponseEntity<ResponseData<List<AdmRight>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/list-parent", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<List<AdmRight>>> getListParent() {
        ResponseData<List<AdmRight>> response = new ResponseData<>();
        List<AdmRight> listRight = null;
        listRight = admRightService.getListParent();
        if (listRight != null) {
            response = new ResponseData<List<AdmRight>>(listRight, Result.SUCCESS);
            return new ResponseEntity<ResponseData<List<AdmRight>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<AdmRight>>(listRight, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<List<AdmRight>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<AdmRight>> findOne(@PathVariable("id") int id) {
        ResponseData<AdmRight> response = new ResponseData<AdmRight>();
        AdmRight admRight = admRightService.getOne(id);
        if (admRight != null) {
            response = new ResponseData<AdmRight>(admRight, Result.SUCCESS);
            return new ResponseEntity<ResponseData<AdmRight>>(response, HttpStatus.OK);
        }
        response = new ResponseData<AdmRight>(admRight, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<AdmRight>>(response, HttpStatus.NO_CONTENT);
    }

//    @GetMapping(value = "/{id}/right-access", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
//    public ResponseEntity<ResponseData<List<AdmAccessRight>>> getAccessByRight(@PathVariable("id") int id) {
//        ResponseData<List<AdmAccessRight>> response = new ResponseData<List<AdmAccessRight>>();
//        List<AdmAccessRight> admAccessRights = admRightService.getAccessRightByRight(id);
//        if (admAccessRights != null) {
//            response = new ResponseData<List<AdmAccessRight>>(admAccessRights, Result.SUCCESS);
//            return new ResponseEntity<ResponseData<List<AdmAccessRight>>>(response, HttpStatus.OK);
//        }
//        response = new ResponseData<List<AdmAccessRight>>(admAccessRights, Result.NO_CONTENT);
//        return new ResponseEntity<ResponseData<List<AdmAccessRight>>>(response, HttpStatus.NO_CONTENT);
//    }
//
//    @GetMapping(value = "/{id}/right-api", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
//    public ResponseEntity<ResponseData<List<AdmApiRight>>> findApiByRight(@PathVariable("id") int id) {
//        ResponseData<List<AdmApiRight>> response = new ResponseData<List<AdmApiRight>>();
//        List<AdmApiRight> admApiRights = admRightService.getApiByRight(id);
//        if (admApiRights != null) {
//            response = new ResponseData<List<AdmApiRight>>(admApiRights, Result.SUCCESS);
//            return new ResponseEntity<ResponseData<List<AdmApiRight>>>(response, HttpStatus.OK);
//        }
//        response = new ResponseData<List<AdmApiRight>>(admApiRights, Result.NO_CONTENT);
//        return new ResponseEntity<ResponseData<List<AdmApiRight>>>(response, HttpStatus.NO_CONTENT);
//    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Integer>> create(@RequestBody AdmRightDTO admRight) {
        Result result = admRightService.create(admRight);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<ResponseData<Integer>> update(@RequestBody AdmRightDTO admRight) {
        Result result = admRightService.update(admRight);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> delete(@PathVariable("id") Integer id) {
        Result result = admRightService.delete(id);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> deleteAllBatch(@RequestParam("entityIds") int[] entityIds) {
        Result result = admRightService.delete(entityIds);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }
}
