package com.savis.categories.endpoint.rest;


import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.StatusProfileHistory;
import com.savis.categories.dao.repository.StatusProfileHistoryRepository;
import com.savis.categories.service.HoSoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/status-history")
public class StatusProfileHistoryResource {

    @Autowired
    HoSoService statusProfileHistoryRepository;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<Page<StatusProfileHistory>>> findOne ( @RequestParam("hsid") Integer hsid, Pageable pageable ) {
        ResponseData<Page<StatusProfileHistory>> response = new ResponseData<Page<StatusProfileHistory>>();
        Page<StatusProfileHistory> lichSuXuLys = statusProfileHistoryRepository.findStatussHistoryById(hsid, pageable);
        if (lichSuXuLys != null) {
            response = new ResponseData<Page<StatusProfileHistory>>(lichSuXuLys, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<StatusProfileHistory>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<StatusProfileHistory>>(lichSuXuLys, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<StatusProfileHistory>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/notdvc", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<Page<StatusProfileHistory>>> findOneNotDVC ( @RequestParam("hsid") Integer hsid, Pageable pageable ) {
        ResponseData<Page<StatusProfileHistory>> response = new ResponseData<Page<StatusProfileHistory>>();
        Page<StatusProfileHistory> lichSuXuLys = statusProfileHistoryRepository.findStatussHistoryNotDVCById(hsid, pageable);
        if (lichSuXuLys != null) {
            response = new ResponseData<Page<StatusProfileHistory>>(lichSuXuLys, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<StatusProfileHistory>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<StatusProfileHistory>>(lichSuXuLys, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<StatusProfileHistory>>>(response, HttpStatus.NO_CONTENT);
    }
}
