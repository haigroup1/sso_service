package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.common.JsonHelper;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.constants.Constants;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.DMThuTucHC;
import com.savis.categories.dao.entity.dto.HoSoSearchDTO;
import com.savis.categories.service.DMThuTucHCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/tthc")
public class DMThuTucHCResource {

    @Autowired
    private DMThuTucHCService dmThuTucHCService;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<List<DMThuTucHC>>> getListByApp( @RequestParam(value = "appid", required = false) Integer appID ) {
        ResponseData<List<DMThuTucHC>> response = new ResponseData<>();
        List<DMThuTucHC> listTTHC = null;
        listTTHC = dmThuTucHCService.listDMThuTucHC(appID);
        if (listTTHC != null) {
            response = new ResponseData<List<DMThuTucHC>>(listTTHC, Result.SUCCESS);
            return new ResponseEntity<ResponseData<List<DMThuTucHC>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<DMThuTucHC>>(listTTHC, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<List<DMThuTucHC>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/app", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<Page<DMThuTucHC>>> getList( @RequestParam(required = false) String search, Pageable pageable ) {
        ResponseData<Page<DMThuTucHC>> response = new ResponseData<>();
        Page<DMThuTucHC> listTTHC = null;
        HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
        if(searchObject.getAppID() != null)
            listTTHC = dmThuTucHCService.listDMThuTucHCByApp(searchObject.getAppID(), pageable);
        else
            listTTHC = dmThuTucHCService.getPage(pageable);
        if (listTTHC != null) {
            response = new ResponseData<Page<DMThuTucHC>>(listTTHC, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<DMThuTucHC>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<DMThuTucHC>>(listTTHC, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<DMThuTucHC>>>(response, HttpStatus.NO_CONTENT);
    }


    @GetMapping(value = "/tthc-detail", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public @ResponseBody ResponseData<DMThuTucHC> getStatusDetail( @RequestParam(required = false) String maThuTuc, @RequestParam(required = false) Integer appid) {
        ResponseData<DMThuTucHC> response = new ResponseData<>();
        DMThuTucHC dmThuTucHC = new DMThuTucHC();
        try {
            dmThuTucHC = dmThuTucHCService.findByMaThuTuc(maThuTuc, appid);
            response.setData(dmThuTucHC);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);
        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

}
