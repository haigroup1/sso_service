package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.common.JsonHelper;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.constants.Constants;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.HoSo;
import com.savis.categories.dao.entity.dto.*;
import com.savis.categories.dao.repository.HoSoRepository;
import com.savis.categories.service.HoSoService;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.jsolve.templ4docx.core.Docx;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/ho-so")
public class HoSoResource {

    @Value("${delete.fax}")
    private String deleteValue;

    @Autowired
    HoSoService hoSoService;

    @Autowired
    HoSoRepository hoSoRepository;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<HoSoResponse>> getPage( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<HoSoResponse>> response = new ResponseData<>();
        HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
        Page<HoSoResponse> hoSos = null ;
        try {
            hoSos = hoSoService.getPage(pageable, searchObject);
            response.setData(hoSos);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResponseData<HoSo>> findOne ( @PathVariable("id") int apiId ) {
        ResponseData<HoSo> response = new ResponseData<HoSo>();
        HoSo hoSo = hoSoService.getOne(apiId);
        if (hoSo != null) {
            response = new ResponseData<HoSo>(hoSo, Result.SUCCESS);
            return new ResponseEntity<ResponseData<HoSo>>(response, HttpStatus.OK);
        }
        response = new ResponseData<HoSo>(hoSo, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<HoSo>>(response, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/amount-status", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<CountStatusProfileDTO>> countLogPage( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<CountStatusProfileDTO>> response = new ResponseData<>();
        HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
        Page<CountStatusProfileDTO> hoSoAmount =null ;
        try {
            hoSoAmount = hoSoService.countStatusProfile(searchObject, pageable);
            response.setData( hoSoAmount);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }


    @RequestMapping(value = "/amount-status/status-date", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<HoSoResponse>> getPageStatusProfileFollowDate( @RequestParam(required = false) String search,
                                                                     @RequestParam(required = false) String type, Pageable pageable) {
        ResponseData<Page<HoSoResponse>> response = new ResponseData<>();
        HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
        Page<HoSoResponse> hoSoPage =null ;
        try {
            hoSoPage = hoSoService.getPageGetLogFollowDate(searchObject, pageable, type);
            response.setData((Page<HoSoResponse>) hoSoPage);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> deletePGML( @RequestParam(required = false) int id) {
        HoSo hoSoDelete = hoSoRepository.findById(id);
        hoSoDelete.setFax(deleteValue);
        ResponseEntity<Integer> response = null;
        try {
            hoSoRepository.save(hoSoDelete);
            response = new ResponseEntity<Integer>(1, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<Integer>(0, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/list-hoso", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> deleteAllPGML( @RequestParam(value = "ids",required = false) int[] entityIds) {
        ResponseEntity<Integer> response = null;
        try {
            for(int i = 0; i < entityIds.length; i++) {
                HoSo hoSoDelete = hoSoRepository.findById(entityIds[i]);
                hoSoDelete.setFax(deleteValue);
                hoSoRepository.save(hoSoDelete);
            }
            response = new ResponseEntity<Integer>(1, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<Integer>(0, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @GetMapping("/list-ho-so/export-file")
    public void exportWord( HttpServletResponse response,@RequestParam(required = false) String search, Pageable pageable) {
        try {
            Integer opptionDownload = 1;
            HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
            List<HoSoResponse>  hoSos = hoSoService.getList(searchObject);
            SimpleDateFormat dateFormatHeader = new SimpleDateFormat("dd?MM/yyyy");
            DateFormat dateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");
            DateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
            response.setHeader("Content-Disposition", "attachment; filename= DanhSachHoSo_" + dateFormatHeader.format(new Date()) + ".docx");
            ServletOutputStream out = response.getOutputStream();
            InputStream resource = new ClassPathResource("/file/templateAuction.docx").getInputStream();
            WordUtil wordUtil = new WordUtil();
            Docx docx = wordUtil.getFileDoc(searchObject, hoSos, resource);

            XWPFDocument documentOutPut = docx.getXWPFDocument();
            if (opptionDownload == 1) {
                PdfOptions options = PdfOptions.create();
                OutputStream outputStream = new FileOutputStream("WordDocument.pdf");
                PdfConverter.getInstance().convert(documentOutPut, outputStream, options);
                response.setHeader("Content-Disposition", "attachment; filename= DanhSachHoSo_" + dateFormatHeader.format(new Date()) + ".pdf");
                InputStreamResource fileResource = new InputStreamResource(new FileInputStream("WordDocument.pdf"));
                InputStream is = fileResource.getInputStream();
                byte[] bytes = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(bytes)) != -1) {
                    // Ghi dữ liệu ảnh vào Response.
                    response.getOutputStream().write(bytes, 0, bytesRead);
                }
                is.close();
                out.flush();
                out.close();
            }
            documentOutPut.write(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/hotich-lylich", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<HoSoResponse>> getPageHoTichAndLyLich( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<HoSoResponse>> response = new ResponseData<>();
        HoSoSearchDTO searchObject = JsonHelper.jsonToObject(search, HoSoSearchDTO.class);
        Page<HoSoResponse> hoSos = null ;
        try {
            hoSos = hoSoService.getPageHoTichAndLyLich(pageable, searchObject);
            response.setData(hoSos);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/status-detail", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<StatusMappingDTO> getStatusDetail( @RequestParam(required = false) Integer appid, @RequestParam(required = false) Integer trangThaiTrenBo) {
        ResponseData<StatusMappingDTO> response = new ResponseData<>();
        StatusMappingDTO statusDetail = new StatusMappingDTO();
        try {
            statusDetail = hoSoService.getStatusDetail(appid, trangThaiTrenBo);
            response.setData(statusDetail);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }
}
