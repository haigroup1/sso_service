package com.savis.categories.endpoint.rest;

import com.savis.categories.auth.dto.adm.role.AdmRoleDTO;
import com.savis.categories.auth.dto.common.ResponseData;
import com.savis.categories.common.exception.Result;
import com.savis.categories.dao.entity.adm.AdmRole;
import com.savis.categories.dao.entity.adm.AdmUser;
import com.savis.categories.endpoint.dto.common.JsonHelper;
import com.savis.categories.service.AdmRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/savis/autho/api/v1/roles")
public class AdmRoleResource {

    @Autowired
    AdmRoleService admRoleService;

    @GetMapping(value = "", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<Page<AdmRole>>> getPage( Pageable pageable,
                                                                @RequestParam(value = "search", required = false) String search,
                                                                @RequestParam(value = "filter", required = false) String filter) {
        ResponseData<Page<AdmRole>> response = new ResponseData<>();
        Page<AdmRole> pageRole = null;
        AdmRole searchObject = JsonHelper.jsonToObject(search, AdmRole.class);

        // advance search
        if (searchObject != null) {
            pageRole = admRoleService.getPage(pageable, searchObject);
        }
        // filter search
        else if (filter != null) {
            pageRole = admRoleService.getPage(pageable, filter);
        }
        // get page default
        else {
            pageRole = admRoleService.getPage(pageable);
        }

        if (pageRole != null) {
            response = new ResponseData<Page<AdmRole>>(pageRole, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<AdmRole>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<AdmRole>>(pageRole, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<AdmRole>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/list", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<List<AdmRole>>> getList() {
        ResponseData<List<AdmRole>> response = new ResponseData<>();
        List<AdmRole> listRole = null;
        listRole = admRoleService.getList();
        if (listRole != null) {
            response = new ResponseData<List<AdmRole>>(listRole, Result.SUCCESS);
            return new ResponseEntity<ResponseData<List<AdmRole>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<AdmRole>>(listRole, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<List<AdmRole>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<AdmRole>> findOne(@PathVariable("id") int id) {
        ResponseData<AdmRole> response = new ResponseData<AdmRole>();
        AdmRole admRole = admRoleService.getOne(id);
        if (admRole != null) {
            response = new ResponseData<AdmRole>(admRole, Result.SUCCESS);
            return new ResponseEntity<ResponseData<AdmRole>>(response, HttpStatus.OK);
        }
        response = new ResponseData<AdmRole>(admRole, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<AdmRole>>(response, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Integer>> create(@RequestBody AdmRoleDTO admRole) {
        Result result = admRoleService.create(admRole);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<ResponseData<Integer>> update(@RequestBody AdmRoleDTO admRole) {
        Result result = admRoleService.update(admRole);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> delete(@PathVariable("id") Integer id) {
        Result result = admRoleService.delete(id);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> deleteAllBatch(@RequestParam("entityIds") int[] entityIds) {
        Result result = admRoleService.delete(entityIds);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }
}
